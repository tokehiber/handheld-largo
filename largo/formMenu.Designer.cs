﻿namespace largo
{
    partial class formMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnReceiving = new System.Windows.Forms.CheckBox();
            this.btnPutaway = new System.Windows.Forms.CheckBox();
            this.btnPicking = new System.Windows.Forms.CheckBox();
            this.btnShipping = new System.Windows.Forms.CheckBox();
            this.btnCycle = new System.Windows.Forms.CheckBox();
            this.btnActiveStock = new System.Windows.Forms.CheckBox();
            this.btnBin = new System.Windows.Forms.CheckBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.btnServer = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Main Menu";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnReceiving
            // 
            this.btnReceiving.Location = new System.Drawing.Point(17, 72);
            this.btnReceiving.Name = "btnReceiving";
            this.btnReceiving.Size = new System.Drawing.Size(100, 20);
            this.btnReceiving.TabIndex = 1;
            this.btnReceiving.Text = "Receiving";
            this.btnReceiving.CheckStateChanged += new System.EventHandler(this.btnReceiving_CheckStateChanged);
            // 
            // btnPutaway
            // 
            this.btnPutaway.Location = new System.Drawing.Point(123, 72);
            this.btnPutaway.Name = "btnPutaway";
            this.btnPutaway.Size = new System.Drawing.Size(100, 20);
            this.btnPutaway.TabIndex = 2;
            this.btnPutaway.Text = "Putaway";
            this.btnPutaway.CheckStateChanged += new System.EventHandler(this.btnPutaway_CheckStateChanged);
            // 
            // btnPicking
            // 
            this.btnPicking.Location = new System.Drawing.Point(17, 98);
            this.btnPicking.Name = "btnPicking";
            this.btnPicking.Size = new System.Drawing.Size(100, 20);
            this.btnPicking.TabIndex = 3;
            this.btnPicking.Text = "Picking";
            this.btnPicking.CheckStateChanged += new System.EventHandler(this.btnPicking_CheckStateChanged);
            // 
            // btnShipping
            // 
            this.btnShipping.Location = new System.Drawing.Point(123, 98);
            this.btnShipping.Name = "btnShipping";
            this.btnShipping.Size = new System.Drawing.Size(100, 20);
            this.btnShipping.TabIndex = 4;
            this.btnShipping.Text = "Shipping";
            this.btnShipping.CheckStateChanged += new System.EventHandler(this.btnShipping_CheckStateChanged);
            // 
            // btnCycle
            // 
            this.btnCycle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnCycle.Location = new System.Drawing.Point(17, 150);
            this.btnCycle.Name = "btnCycle";
            this.btnCycle.Size = new System.Drawing.Size(100, 20);
            this.btnCycle.TabIndex = 6;
            this.btnCycle.Text = "Cycle Count";
            this.btnCycle.CheckStateChanged += new System.EventHandler(this.btnCycle_CheckStateChanged);
            // 
            // btnActiveStock
            // 
            this.btnActiveStock.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnActiveStock.Location = new System.Drawing.Point(123, 150);
            this.btnActiveStock.Name = "btnActiveStock";
            this.btnActiveStock.Size = new System.Drawing.Size(100, 20);
            this.btnActiveStock.TabIndex = 7;
            this.btnActiveStock.Text = "Active Stock";
            this.btnActiveStock.CheckStateChanged += new System.EventHandler(this.btnActiveStock_CheckStateChanged);
            // 
            // btnBin
            // 
            this.btnBin.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnBin.Location = new System.Drawing.Point(17, 176);
            this.btnBin.Name = "btnBin";
            this.btnBin.Size = new System.Drawing.Size(100, 20);
            this.btnBin.TabIndex = 8;
            this.btnBin.Text = "Bin Transfer";
            this.btnBin.CheckStateChanged += new System.EventHandler(this.btnBin_CheckStateChanged);
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(84, 228);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(72, 20);
            this.btnLogout.TabIndex = 10;
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnServer
            // 
            this.btnServer.Location = new System.Drawing.Point(123, 176);
            this.btnServer.Name = "btnServer";
            this.btnServer.Size = new System.Drawing.Size(100, 20);
            this.btnServer.TabIndex = 9;
            this.btnServer.Text = "Setting";
            this.btnServer.Click += new System.EventHandler(this.btnServer_Click);
            // 
            // formMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnServer);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnBin);
            this.Controls.Add(this.btnActiveStock);
            this.Controls.Add(this.btnCycle);
            this.Controls.Add(this.btnShipping);
            this.Controls.Add(this.btnPicking);
            this.Controls.Add(this.btnPutaway);
            this.Controls.Add(this.btnReceiving);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formMenu";
            this.Text = "Main Menu";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formMenu_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox btnReceiving;
        private System.Windows.Forms.CheckBox btnPutaway;
        private System.Windows.Forms.CheckBox btnPicking;
        private System.Windows.Forms.CheckBox btnShipping;
        private System.Windows.Forms.CheckBox btnCycle;
        private System.Windows.Forms.CheckBox btnActiveStock;
        private System.Windows.Forms.CheckBox btnBin;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.CheckBox btnServer;
    }
}