﻿namespace largo
{
    partial class formPickingCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.fieldPickingCode = new System.Windows.Forms.TextBox();
            this.tblPicking = new System.Windows.Forms.DataGrid();
            this.btnScan = new System.Windows.Forms.Button();
            this.btnLocation = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Picking By Picking Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 24);
            this.label2.Text = "Picking Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldPickingCode
            // 
            this.fieldPickingCode.Location = new System.Drawing.Point(45, 73);
            this.fieldPickingCode.Name = "fieldPickingCode";
            this.fieldPickingCode.Size = new System.Drawing.Size(150, 23);
            this.fieldPickingCode.TabIndex = 0;
            this.fieldPickingCode.TextChanged += new System.EventHandler(this.fieldPickingCode_TextChanged);
            this.fieldPickingCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldPickingCode_KeyPress);
            // 
            // tblPicking
            // 
            this.tblPicking.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tblPicking.Location = new System.Drawing.Point(20, 102);
            this.tblPicking.Name = "tblPicking";
            this.tblPicking.RowHeadersVisible = false;
            this.tblPicking.Size = new System.Drawing.Size(200, 150);
            this.tblPicking.TabIndex = 1;
            this.tblPicking.CurrentCellChanged += new System.EventHandler(this.tblPicking_CurrentCellChanged);
            // 
            // btnScan
            // 
            this.btnScan.Enabled = false;
            this.btnScan.Location = new System.Drawing.Point(20, 258);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(72, 20);
            this.btnScan.TabIndex = 2;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // btnLocation
            // 
            this.btnLocation.Enabled = false;
            this.btnLocation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnLocation.Location = new System.Drawing.Point(20, 284);
            this.btnLocation.Name = "btnLocation";
            this.btnLocation.Size = new System.Drawing.Size(72, 20);
            this.btnLocation.TabIndex = 4;
            this.btnLocation.Text = "Location List";
            this.btnLocation.Click += new System.EventHandler(this.btnLocation_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Location = new System.Drawing.Point(148, 258);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(72, 20);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // formPickingCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnLocation);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.tblPicking);
            this.Controls.Add(this.fieldPickingCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formPickingCode";
            this.Text = "Picking By Picking Code";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formPickingCode_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fieldPickingCode;
        private System.Windows.Forms.DataGrid tblPicking;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Button btnLocation;
        private System.Windows.Forms.Button btnSubmit;
    }
}