﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace largo
{
    public partial class formSelectPutaway : Form
    {
        private formPutaway menuPutaway;

        public formSelectPutaway()
        {
            InitializeComponent();
            menuPutaway = new formPutaway();
        }

        private void putwayOutbound_CheckStateChanged(object sender, EventArgs e)
        {
            menuPutaway.setLocation("OUTBOUND");
            menuPutaway.Show();
        }

        private void putwayIntransfer_CheckStateChanged(object sender, EventArgs e)
        {
            menuPutaway.setLocation("IN-TRANSFER");
            menuPutaway.Show();
        }

        private void putwayInbound_CheckStateChanged(object sender, EventArgs e)
        {
            menuPutaway.setLocation("INBOUND");
            menuPutaway.Show();
        }

        private void formSelectPutaway_Activated(object sender, EventArgs e)
        {
            putwayInbound.Checked = false;
            putwayOutbound.Checked = false;
            putwayIntransfer.Checked = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            menuPutaway.Hide();
            this.Hide();
        }
    }
}