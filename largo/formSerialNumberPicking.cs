﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formSerialNumberPicking : Form
    {
        private string pickingCode;
        private string itemCode;
        private string serialNumber;

        public formSerialNumberPicking()
        {
            InitializeComponent();
        }

        public void setPicking(string picking, string item)
        {
            pickingCode = picking;
            itemCode = item;
        }

        private void setDoneButton()
        {
            int n = db.pickingCodeDB.getByItem(pickingCode, itemCode, true).Tables[0].Rows.Count;
            int N = db.pickingCodeDB.getByItem(pickingCode, itemCode, false).Tables[0].Rows.Count;
            if ((n < N) && (N != 0))
            {
                btnDone.Enabled = false;
            }
            else
            {
                btnDone.Enabled = true;
            }
        }

        private void formSerialNumberPicking_Activated(object sender, EventArgs e)
        {
            lblTitle.Text = pickingCode + ": " + itemCode;
            tblSerialNumber.DataSource = controllers.pickingCodeController.getByPickingItemCode(pickingCode, itemCode);
            this.setDoneButton();
            fieldSerialNumber.Focus();
        }

        private void tblSerialNumber_CurrentCellChanged(object sender, EventArgs e)
        {
            tblSerialNumber.Select(tblSerialNumber.CurrentRowIndex);
            btnDelete.Enabled = true;
            serialNumber = tblSerialNumber[tblSerialNumber.CurrentCell.RowNumber, 2].ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure to remove " + serialNumber + "?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                db.pickingCodeDB.delete(pickingCode, itemCode, serialNumber);
                tblSerialNumber.DataSource = controllers.pickingCodeController.getByPickingItemCode(pickingCode, itemCode);
            }
            this.setDoneButton();
            fieldSerialNumber.Focus();
            btnDelete.Enabled = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            int n = db.pickingCodeDB.getByItem(pickingCode, itemCode, true).Tables[0].Rows.Count;
            int N = db.pickingCodeDB.getByItem(pickingCode, itemCode, false).Tables[0].Rows.Count;
            if (n < N)
            {
                DialogResult konfirmasi = MessageBox.Show("Are you sure to exit?\n" + n + " of " + N + " item(s) picked.", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    this.Hide();
                }
                btnDelete.Enabled = false;
            }
            else
            {
                this.Hide();
            }
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    int n = db.pickingCodeDB.getByItem(pickingCode, itemCode, true).Tables[0].Rows.Count;
                    int N = db.pickingCodeDB.getByItem(pickingCode, itemCode, false).Tables[0].Rows.Count;
                    if (n < N)
                    {
                        string serial = fieldSerialNumber.Text;
                        int count = db.pickingCodeDB.getBySerialNumber(serial).Tables[0].Rows.Count;
                        if (count > 0)
                        {
                            MessageBox.Show(serial + " has been scanned before.", "Error: 401");
                        }
                        else
                        {
                            string url = controllers.serverController.getServerAddress() + apiLists.isValidSNPicking + itemCode + "/" + serial + "/";
                            apiAgent.get(url, parseSerialNumberResponse, true);
                        }
                    }
                    else
                    {
                        MessageBox.Show("It should be enough.\nScanned item: " + n + " of " + N + ".", pickingCode + ": " + itemCode);
                    }
                    this.setDoneButton();
                    fieldSerialNumber.Text = "";
                    fieldSerialNumber.Focus();
                }
            }
        }

        private void parseSerialNumberResponse(string response)
        {
            models.serialNumberModel jsonResponse = JsonConvert.DeserializeObject<models.serialNumberModel>(response);
            if (jsonResponse.response)
            {
                db.pickingCodeDB.update(pickingCode, itemCode, jsonResponse.param);
                tblSerialNumber.DataSource = controllers.pickingCodeController.getByPickingItemCode(pickingCode, itemCode);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data has been saved.", pickingCode + ": " + itemCode);
            this.Hide();
        }

        private void formSerialNumberPicking_Deactivate(object sender, EventArgs e)
        {
            btnDone.Enabled = false;
        }
    }
}