﻿namespace largo
{
    partial class formSerialNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.fieldSerialNumber = new System.Windows.Forms.TextBox();
            this.tblSerialNumber = new System.Windows.Forms.DataGrid();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.lblBatchCode = new System.Windows.Forms.Label();
            this.fieldBatchCode = new System.Windows.Forms.TextBox();
            this.lblExpDate = new System.Windows.Forms.Label();
            this.fieldExpireDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(3, 4);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(234, 24);
            this.lblTitle.Text = "Title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.Location = new System.Drawing.Point(3, 37);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(234, 20);
            this.lblSerialNumber.Text = "Serial Number";
            this.lblSerialNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldSerialNumber
            // 
            this.fieldSerialNumber.Location = new System.Drawing.Point(45, 52);
            this.fieldSerialNumber.Name = "fieldSerialNumber";
            this.fieldSerialNumber.Size = new System.Drawing.Size(150, 23);
            this.fieldSerialNumber.TabIndex = 0;
            this.fieldSerialNumber.GotFocus += new System.EventHandler(this.fieldSerialNumber_GotFocus);
            this.fieldSerialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldSerialNumber_KeyPress);
            // 
            // tblSerialNumber
            // 
            this.tblSerialNumber.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tblSerialNumber.Location = new System.Drawing.Point(20, 156);
            this.tblSerialNumber.Name = "tblSerialNumber";
            this.tblSerialNumber.RowHeadersVisible = false;
            this.tblSerialNumber.Size = new System.Drawing.Size(200, 115);
            this.tblSerialNumber.TabIndex = 3;
            this.tblSerialNumber.CurrentCellChanged += new System.EventHandler(this.tblSerialNumber_CurrentCellChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(20, 278);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(72, 20);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnDone
            // 
            this.btnDone.Enabled = false;
            this.btnDone.Location = new System.Drawing.Point(148, 278);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(72, 20);
            this.btnDone.TabIndex = 5;
            this.btnDone.Text = "Done";
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // lblBatchCode
            // 
            this.lblBatchCode.Location = new System.Drawing.Point(3, 74);
            this.lblBatchCode.Name = "lblBatchCode";
            this.lblBatchCode.Size = new System.Drawing.Size(234, 20);
            this.lblBatchCode.Text = "Batch Code";
            this.lblBatchCode.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldBatchCode
            // 
            this.fieldBatchCode.Enabled = false;
            this.fieldBatchCode.Location = new System.Drawing.Point(45, 89);
            this.fieldBatchCode.Name = "fieldBatchCode";
            this.fieldBatchCode.Size = new System.Drawing.Size(150, 23);
            this.fieldBatchCode.TabIndex = 1;
            this.fieldBatchCode.GotFocus += new System.EventHandler(this.fieldBatchCode_GotFocus);
            // 
            // lblExpDate
            // 
            this.lblExpDate.Location = new System.Drawing.Point(3, 111);
            this.lblExpDate.Name = "lblExpDate";
            this.lblExpDate.Size = new System.Drawing.Size(234, 20);
            this.lblExpDate.Text = "Expire Date";
            this.lblExpDate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldExpireDate
            // 
            this.fieldExpireDate.CustomFormat = "MMM, dd yyyy";
            this.fieldExpireDate.Enabled = false;
            this.fieldExpireDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fieldExpireDate.Location = new System.Drawing.Point(45, 126);
            this.fieldExpireDate.Name = "fieldExpireDate";
            this.fieldExpireDate.Size = new System.Drawing.Size(150, 24);
            this.fieldExpireDate.TabIndex = 2;
            this.fieldExpireDate.GotFocus += new System.EventHandler(this.fieldExpireDate_GotFocus);
            // 
            // formSerialNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.fieldBatchCode);
            this.Controls.Add(this.fieldSerialNumber);
            this.Controls.Add(this.fieldExpireDate);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lblSerialNumber);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblExpDate);
            this.Controls.Add(this.tblSerialNumber);
            this.Controls.Add(this.lblBatchCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formSerialNumber";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formSerialNumber_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.TextBox fieldSerialNumber;
        private System.Windows.Forms.DataGrid tblSerialNumber;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label lblBatchCode;
        private System.Windows.Forms.TextBox fieldBatchCode;
        private System.Windows.Forms.Label lblExpDate;
        private System.Windows.Forms.DateTimePicker fieldExpireDate;
    }
}