﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace largo
{
    public partial class formSetting : Form
    {
        private formServer menuServer;
        private formLocation menuLocation;

        public formSetting()
        {
            InitializeComponent();
            menuServer = new formServer();
            menuLocation = new formLocation();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void formSetting_Activated(object sender, EventArgs e)
        {
            btnServer.Checked = false;
            btnLocation.Checked = false;
        }

        private void btnServer_Click(object sender, EventArgs e)
        {
            menuServer.Show();
        }

        private void btnLocation_Click(object sender, EventArgs e)
        {
            menuLocation.Show();
        }
    }
}