﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.Net;

namespace largo
{
    public partial class formLogin : Form
    {
        private static formLogin instance;
        private formServer menuServer;

        public static formLogin getInstance()
        {
            if (instance == null)
            {
                instance = new formLogin();
            }

            return instance;
        }

        public formLogin()
        {
            InitializeComponent();
            menuServer = new formServer();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string server = controllers.serverController.getServerAddress();
            if (string.IsNullOrEmpty(server))
            {
                MessageBox.Show("Server address is never assign before. Please assign it now.");
                menuServer.Show();
            }
            else
            {
                string url = server + apiLists.login;
                string uname = fieldUname.Text;
                string upass = fieldUpass.Text;
                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                httpParams.Add(new KeyValuePair<string, string>("uname", uname));
                httpParams.Add(new KeyValuePair<string, string>("upass", upass));

                apiAgent.post(url, httpParams, parseJsonResponse, false);
            }
        }

        private void parseJsonResponse(string response)
        {
            models.loginModel jsonResponse = JsonConvert.DeserializeObject<models.loginModel>(response);
            if (jsonResponse.response)
            {
                controllers.userController.setUserInfo(jsonResponse.uid, jsonResponse.uname, jsonResponse.handheld_session_code);
                this.Hide();
                formMenu.getInstance().Show();
            }
            else
            {
                MessageBox.Show("Username or password is incorrect");
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void fieldUname_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(fieldUname.Text) && !string.IsNullOrEmpty(fieldUpass.Text))
            {
                btnLogin.Enabled = true;
            }
            else
            {
                btnLogin.Enabled = false;
            }

        }

        private void fieldUpass_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(fieldUname.Text) && !string.IsNullOrEmpty(fieldUpass.Text))
            {
                btnLogin.Enabled = true;
            }
            else
            {
                btnLogin.Enabled = false;
            }
        }
    }
}