﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formProsesTransfer : Form
    {
        private string transferCode;
        private string serialNumber;

        public formProsesTransfer()
        {
            InitializeComponent();
        }

        public void setTransferCode(string transfer)
        {
            transferCode = transfer;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Title";
            fieldNewLocation.Text = "";
            fieldSerialNumber.Text = "";
            tblTransfer.DataSource = null;
            tblTransfer.Refresh();
            btnDelete.Enabled = false;
            btnSubmit.Enabled = false;
            this.Hide();
        }

        private void formProsesTransfer_Activated(object sender, EventArgs e)
        {
            lblTitle.Text = transferCode;
            DataSet dataSet = db.transferDB.getByTransferCode(transferCode, false);
            tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
            DataSet dataSetPutted = db.transferDB.getByTransferCode(transferCode, true);
            if (dataSetPutted.Tables[0].Rows.Count != 0)
            {
                btnSubmit.Enabled = false;
            }
            else
            {
                btnSubmit.Enabled = true;
            }
        }

        private void fieldNewLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldNewLocation.Text))
                {
                    string location = fieldNewLocation.Text;
                    string url = controllers.serverController.getServerAddress() + apiLists.isValidLocation + location + "/";
                    apiAgent.get(url, jsonResponseLocation, true);
                }
            }
        }

        private void jsonResponseLocation(string response)
        {
            models.locationModel jsonResponse = JsonConvert.DeserializeObject<models.locationModel>(response);
            if (jsonResponse.response)
            {
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldNewLocation.Focus();
            }
        }

        private void fieldNewLocation_TextChanged(object sender, EventArgs e)
        {
            fieldSerialNumber.Enabled = false;
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string serial = fieldSerialNumber.Text;
                    DataSet dataSet = db.transferDB.getBySerialNumber(transferCode, serial);
                    int count = dataSet.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        db.transferDB.updatePickedSerial(transferCode, serial, fieldNewLocation.Text, controllers.userController.getUserInfo("uname"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        DataSet dataSet1 = db.transferDB.getByTransferCode(transferCode, false);
                        tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet1);
                        tblTransfer.Refresh();
                        btnSubmit.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Invalid serial number", "Warning!");
                    }
                }
                fieldSerialNumber.Text = "";
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                db.transferDB.removePickedSerial(transferCode, serialNumber);
                DataSet dataSet = db.transferDB.getByTransferCode(transferCode, false);
                tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
                tblTransfer.Refresh();
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    btnSubmit.Enabled = true;
                }
                else
                {
                    btnSubmit.Enabled = false;
                }
            }
            btnDelete.Enabled = false;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            DataSet datan = db.transferDB.getByTransferCode(transferCode, true);
            DataSet dataN = db.transferDB.getByTransferCode(transferCode, false);
            int n = datan.Tables[0].Rows.Count;
            int N = dataN.Tables[0].Rows.Count;
            if (n < N)
            {
                MessageBox.Show(N-n + " item(s) left.", "Warning!");
            }
            else
            {
                string url = controllers.serverController.getServerAddress() + apiLists.setTransfer;
                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                for (int i = 0; i < N; i++)
                {
                    string transfer = dataN.Tables[0].Rows[i]["transfer_code"].ToString();
                    string serial = dataN.Tables[0].Rows[i]["kd_unik"].ToString();
                    string oldLoc = dataN.Tables[0].Rows[i]["loc_name_old"].ToString();
                    string unamePick = dataN.Tables[0].Rows[i]["user_name_pick"].ToString();
                    string newLoc = dataN.Tables[0].Rows[i]["loc_name_new"].ToString();
                    string unamePut = dataN.Tables[0].Rows[i]["user_name_put"].ToString();
                    string putTime = dataN.Tables[0].Rows[i]["put_time"].ToString();
                    httpParams.Add(new KeyValuePair<string, string>("transfer_code", transfer));
                    httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                    httpParams.Add(new KeyValuePair<string, string>("uname_pick", unamePick));
                    httpParams.Add(new KeyValuePair<string, string>("uname_put", unamePut));
                    httpParams.Add(new KeyValuePair<string, string>("loc_name_old", oldLoc));
                    httpParams.Add(new KeyValuePair<string, string>("loc_name_new", newLoc));
                    httpParams.Add(new KeyValuePair<string, string>("put_time", putTime));
                    apiAgent.post(url, httpParams, parseSubmitBySerialResponse, true);
                }
                url = controllers.serverController.getServerAddress() + apiLists.lockTransfer;
                httpParams.Add(new KeyValuePair<string, string>("transfer_code", transferCode));
                apiAgent.post(url, httpParams, parseSubmitResponse, true);
                MessageBox.Show("Sumbit success.");
                this.Hide();
            }
        }

        private void parseSubmitBySerialResponse(string response)
        {
            models.transferModel jsonResponse = JsonConvert.DeserializeObject<models.transferModel>(response);
            if (jsonResponse.response)
            {
                db.transferDB.deletePickedSerial(jsonResponse.transfer, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseSubmitResponse(string response)
        {
            models.transferModel jsonResponse = JsonConvert.DeserializeObject<models.transferModel>(response);
            if (jsonResponse.response)
            {
                db.transferDB.deleteByTransferCode(jsonResponse.transfer);
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void tblTransfer_CurrentCellChanged(object sender, EventArgs e)
        {
            tblTransfer.Select(tblTransfer.CurrentRowIndex);
            int row = tblTransfer.CurrentCell.RowNumber;
            serialNumber = tblTransfer[row, 1].ToString();
            btnDelete.Enabled = true;
        }
    }
}