﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formShipping : Form
    {
        private string shippingCode;
        private string itemCode;
        private string serialNumber;

        public formShipping()
        {
            InitializeComponent();
        }

        private void setShipping(string shipping, string item, string serial)
        {
            shippingCode = shipping;
            itemCode = item;
            serialNumber = serial;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fieldShippingCode_TextChanged(object sender, EventArgs e)
        {
            fieldSerialNumber.Enabled = false;
            tblShipping.DataSource = null;
            tblShipping.Refresh();
            btnDelete.Enabled = false;
            btnSubmit.Enabled = false;
        }

        private void fieldShippingCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldShippingCode.Text))
                {
                    string shipping = fieldShippingCode.Text;
                    DataSet data = db.shippingDB.getByShipping(shipping, false);
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        fieldSerialNumber.Enabled = true;
                        fieldSerialNumber.Focus();
                        tblShipping.DataSource = controllers.shippingController.getTableByDataShipping(data);
                        int n = db.shippingDB.getByShipping(shipping, true).Tables[0].Rows.Count;
                        if (n > 0 && n == data.Tables[0].Rows.Count)
                        {
                            btnSubmit.Enabled = true;
                        }
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.getShipping + shipping + "/";
                        apiAgent.get(url, parseShippingResponse, true);
                    }
                }
            }
        }

        private void parseShippingResponse(string response)
        {
            models.shippingModel jsonResponse = JsonConvert.DeserializeObject<models.shippingModel>(response);
            if (jsonResponse.response)
            {
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
                foreach (var asdf in jsonResponse.results)
                {
                    string shipping = asdf.shipping_code;
                    string item = asdf.kd_barang;
                    string serial = asdf.kd_unik;
                    string location = asdf.loc_name;
                    db.shippingDB.insert(shipping, item, serial, location);
                }
                DataSet data = db.shippingDB.getByShipping(jsonResponse.param, false);
                tblShipping.DataSource = controllers.shippingController.getTableByDataShipping(data);
                int n = db.shippingDB.getByShipping(jsonResponse.param, true).Tables[0].Rows.Count;
                if (n > 0 && n == data.Tables[0].Rows.Count)
                {
                    btnSubmit.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldShippingCode.Text = "";
            }
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string shipping = fieldShippingCode.Text;
                    string serial = fieldSerialNumber.Text;
                    if (db.shippingDB.getBySerial(serial).Tables[0].Rows.Count > 0)
                    {
                        db.shippingDB.update(serial, "ship");
                    }
                    else
                    {
                        MessageBox.Show(serial + " is not valid.", "Invalid Serial Number!");
                    }
                    DataSet data = db.shippingDB.getByShipping(shipping, false);
                    tblShipping.DataSource = controllers.shippingController.getTableByDataShipping(data);
                    int n = db.shippingDB.getByShipping(shipping, true).Tables[0].Rows.Count;
                    if (n > 0 && n == data.Tables[0].Rows.Count)
                    {
                        btnSubmit.Enabled = true;
                    }
                    fieldSerialNumber.Text = "";
                }
            }
        }

        private void tblShipping_CurrentCellChanged(object sender, EventArgs e)
        {
            if (tblShipping[tblShipping.CurrentCell.RowNumber, 3].ToString() == "YES")
            {
                btnDelete.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
            }
            tblShipping.Select(tblShipping.CurrentRowIndex);
            string shipping = fieldShippingCode.Text;
            string item = tblShipping[tblShipping.CurrentCell.RowNumber, 1].ToString();
            string serial = tblShipping[tblShipping.CurrentCell.RowNumber, 2].ToString();
            this.setShipping(shipping, item, serial);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            db.shippingDB.update(serialNumber, "remove");
            DataSet data = db.shippingDB.getByShipping(shippingCode, false);
            tblShipping.DataSource = controllers.shippingController.getTableByDataShipping(data);
            tblShipping.Refresh();
            btnDelete.Enabled = false;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            shippingCode = fieldShippingCode.Text;
            List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
            string urlSet = controllers.serverController.getServerAddress() + apiLists.setShipping;
            string urlLock = controllers.serverController.getServerAddress() + apiLists.lockShipping;
            DataSet data = db.shippingDB.getByShipping(shippingCode, true);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                string shipping = data.Tables[0].Rows[i]["kd_shipping"].ToString();
                string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                string user = data.Tables[0].Rows[i]["user_name_shipping"].ToString();
                httpParams.Add(new KeyValuePair<string, string>("shipping_code", shipping));
                httpParams.Add(new KeyValuePair<string, string>("item_code", item));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("uname_ship", user));
                apiAgent.post(urlSet, httpParams, parseJsonSubmitBySerialResponse, true);
            }
            List<KeyValuePair<string, string>> httpParamsLock = new List<KeyValuePair<string, string>>();
            httpParamsLock.Add(new KeyValuePair<string, string>("shipping_code", shippingCode));
            httpParamsLock.Add(new KeyValuePair<string, string>("shipping_date", DateTime.Now.ToString("yyyy-MM-dd")));
            apiAgent.post(urlLock, httpParamsLock, parseJsonSubmitLockResponse, true);
        }

        private void parseJsonSubmitBySerialResponse(string response)
        {
            models.shippingModel jsonResponse = JsonConvert.DeserializeObject<models.shippingModel>(response);
            if (jsonResponse.response)
            {
                db.shippingDB.remove(shippingCode, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseJsonSubmitLockResponse(string response)
        {
            models.pickingCodeModel jsonResponse = JsonConvert.DeserializeObject<models.pickingCodeModel>(response);
            if (jsonResponse.response)
            {
                MessageBox.Show("Data has been updated.", jsonResponse.param);
                fieldShippingCode.Text = "";
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }
    }
}