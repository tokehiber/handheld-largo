﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formActiveDetail : Form
    {
        private string itemCode;

        public formActiveDetail()
        {
            InitializeComponent();
        }

        public void setItemCode(string item)
        {
            itemCode = item;
        }

        private void formActiveDetail_Activated(object sender, EventArgs e)
        {
            string url = controllers.serverController.getServerAddress() + apiLists.activeDetail + itemCode + "/";
            apiAgent.get(url, parseJsonActiveDetailResponse, true);
        }

        private void parseJsonActiveDetailResponse(string response)
        {
            models.activeDetailModel jsonResponse = JsonConvert.DeserializeObject<models.activeDetailModel>(response);
            if (jsonResponse.response)
            {
                lblName.Text = jsonResponse.item_name;
                lblItemCode.Text = jsonResponse.param;
                lblQTY.Text = jsonResponse.qty.ToString();
                DataTable data = new DataTable();
                data.Columns.Add("No.", typeof(int));
                data.Columns.Add("Location", typeof(string));
                data.Columns.Add("QTY", typeof(int));
                int no = 0;
                foreach (var asdf in jsonResponse.results)
                {
                    no += 1;
                    data.Rows.Add(no, asdf.loc_name, asdf.qty);
                }
                tblActiveDetail.DataSource = data;
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void tblActiveDetail_CurrentCellChanged(object sender, EventArgs e)
        {
            tblActiveDetail.Select(tblActiveDetail.CurrentRowIndex);
        }
    }
}