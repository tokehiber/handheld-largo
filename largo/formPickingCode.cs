﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formPickingCode : Form
    {
        private string pickingCode;
        private formPickingLocation menuPickingLocation;
        private formSerialNumberPicking menuSerialNumber;

        public formPickingCode()
        {
            InitializeComponent();
            menuPickingLocation = new formPickingLocation();
            menuSerialNumber = new formSerialNumberPicking();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void setSubmitButton()
        {
            int n = db.pickingCodeDB.getByPicking(pickingCode, true).Tables[0].Rows.Count;
            int N = db.pickingCodeDB.getByPicking(pickingCode, false).Tables[0].Rows.Count;
            if ((n < N) && (N != 0))
            {
                btnSubmit.Enabled = false;
            }
            else
            {
                btnSubmit.Enabled = true;
            }
        }

        private void fieldPickingCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldPickingCode.Text))
                {
                    string picking = fieldPickingCode.Text;
                    DataSet data = db.pickingCodeDB.getByPicking(picking, false);
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        pickingCode = picking;
                        tblPicking.DataSource = controllers.pickingCodeController.getByPickingCode(picking);
                        btnLocation.Enabled = true;
                        this.setSubmitButton();
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.apiPickingCode + picking + "/";
                        apiAgent.get(url, parsePickingResponse, true);
                    }
                }
            }
        }

        private void parsePickingResponse(string response)
        {
            models.pickingCodeModel jsonResponse = JsonConvert.DeserializeObject<models.pickingCodeModel>(response);
            if (jsonResponse.response)
            {
                pickingCode = jsonResponse.param;
                foreach (var asdf in jsonResponse.results)
                {
                    for (int i = 0; i < asdf.qty; i++)
                    {
                        string picking = asdf.pl_name;
                        string item = asdf.kd_barang;
                        db.pickingCodeDB.insert(picking, item);
                    }
                }
                foreach (var asdf in jsonResponse.loc_list)
                {
                    string picking = asdf.pl_name;
                    string barang = asdf.kd_barang;
                    string loc = asdf.loc_name;
                    int qty = asdf.qty;
                    db.pickingCodeDB.insertLocation(picking, barang, loc, qty);
                }
                tblPicking.DataSource = controllers.pickingCodeController.getByPickingCode(jsonResponse.param);
                btnLocation.Enabled = true;
                this.setSubmitButton();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldPickingCode.Text = "";
            }
        }

        private void tblPicking_CurrentCellChanged(object sender, EventArgs e)
        {
            tblPicking.Select(tblPicking.CurrentRowIndex);
            btnScan.Enabled = true;
            string item = tblPicking[tblPicking.CurrentCell.RowNumber, 1].ToString();
            menuSerialNumber.setPicking(pickingCode, item);
        }

        private void fieldPickingCode_TextChanged(object sender, EventArgs e)
        {
            tblPicking.DataSource = null;
            tblPicking.Refresh();
            btnLocation.Enabled = false;
            btnScan.Enabled = false;
            btnSubmit.Enabled = false;
        }

        private void btnLocation_Click(object sender, EventArgs e)
        {
            menuPickingLocation.setPickingCode(pickingCode);
            menuPickingLocation.Show();
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            menuSerialNumber.Show();
        }

        private void formPickingCode_Activated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(fieldPickingCode.Text))
            {
                tblPicking.DataSource = controllers.pickingCodeController.getByPickingCode(fieldPickingCode.Text);
                this.setSubmitButton();
            }
            fieldPickingCode.Focus();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            pickingCode = fieldPickingCode.Text;
            string urlSet = controllers.serverController.getServerAddress() + apiLists.setPicking;
            string urlLock = controllers.serverController.getServerAddress() + apiLists.lockPicking;
            DataSet data = db.pickingCodeDB.getByPicking(pickingCode, true);
            int count = data.Tables[0].Rows.Count;
            for (int i = 0; i < count; i++)
            {
                string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                string uname = data.Tables[0].Rows[i]["user_name_picking"].ToString();
                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                httpParams.Add(new KeyValuePair<string, string>("picking_code", pickingCode));
                httpParams.Add(new KeyValuePair<string, string>("item_code", item));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("uname_pick", uname));
                apiAgent.post(urlSet, httpParams, parseJsonSubmitBySerialResponse, true);
            }
            List<KeyValuePair<string, string>> httpParamsLock = new List<KeyValuePair<string, string>>();
            httpParamsLock.Add(new KeyValuePair<string, string>("picking_code", pickingCode));
            httpParamsLock.Add(new KeyValuePair<string, string>("picking_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            apiAgent.post(urlLock, httpParamsLock, parseJsonSubmitLockResponse, true);
        }

        private void parseJsonSubmitBySerialResponse(string response)
        {
            models.pickingCodeModel jsonResponse = JsonConvert.DeserializeObject<models.pickingCodeModel>(response);
            if (jsonResponse.response)
            {
                db.pickingCodeDB.remove(pickingCode, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseJsonSubmitLockResponse(string response)
        {
            models.pickingCodeModel jsonResponse = JsonConvert.DeserializeObject<models.pickingCodeModel>(response);
            if (jsonResponse.response)
            {
                db.pickingCodeDB.removeLocation(jsonResponse.param);
                MessageBox.Show("Data has been updated.", jsonResponse.param);
                fieldPickingCode.Text = "";
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }
    }
}