﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace largo
{
    public partial class formPickingLocation : Form
    {
        private string pickingCode;

        public formPickingLocation()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void setPickingCode(string picking)
        {
            pickingCode = picking;
        }

        private void formPickingLocation_Activated(object sender, EventArgs e)
        {
            lblTitle.Text = pickingCode;
            tblPickingLocation.DataSource = controllers.pickingCodeController.getLocationByPickingCode(pickingCode);
        }
    }
}