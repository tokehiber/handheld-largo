﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace largo
{
    public partial class formMenu : Form
    {
        private static formMenu instance;
        private controllers.menuController controller;
        private formReceiving menuReceiving;
        private formSelectPutaway menuPutaway;
        private formSetting menuSetting;
        private formTransfer menuTransfer;
        private formPicking menuPicking;
        private formActiveStock menuActiveStock;
        private formShipping menuShipping;
        private formCycleCount menuCycle;

        public static formMenu getInstance()
        {
            if (instance == null)
            {
                instance = new formMenu();
            }

            return instance;
        }

        public formMenu()
        {
            InitializeComponent();
            controller = new controllers.menuController();
            if (controller.isUserValid())
            {
                menuReceiving = new formReceiving();
                menuPutaway = new formSelectPutaway();
                menuSetting = new formSetting();
                menuTransfer = new formTransfer();
                menuPicking = new formPicking();
                menuActiveStock = new formActiveStock();
                menuShipping = new formShipping();
                menuCycle = new formCycleCount();
            }
            else
            {
                formLogin.getInstance().ShowDialog();
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            controllers.userController.removeSession();
            Application.Exit();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnReceiving_CheckStateChanged(object sender, EventArgs e)
        {
            menuReceiving.Show();
        }

        private void formMenu_Activated(object sender, EventArgs e)
        {
            btnReceiving.Checked = false;
            btnPutaway.Checked = false;
            btnPicking.Checked = false;
            btnShipping.Checked = false;
            btnCycle.Checked = false;
            btnActiveStock.Checked = false;
            btnBin.Checked = false;
            btnServer.Checked = false;
        }

        private void btnPutaway_CheckStateChanged(object sender, EventArgs e)
        {
            menuPutaway.Show();
        }

        private void btnServer_Click(object sender, EventArgs e)
        {
            menuSetting.Show();
        }

        private void btnBin_CheckStateChanged(object sender, EventArgs e)
        {
            menuTransfer.Show();
        }

        private void btnPicking_CheckStateChanged(object sender, EventArgs e)
        {
            menuPicking.Show();
        }

        private void btnActiveStock_CheckStateChanged(object sender, EventArgs e)
        {
            menuActiveStock.Show();
        }

        private void btnShipping_CheckStateChanged(object sender, EventArgs e)
        {
            menuShipping.Show();
        }

        private void btnCycle_CheckStateChanged(object sender, EventArgs e)
        {
            menuCycle.Show();
        }
    }
}