﻿namespace largo
{
    partial class formBatchCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.fieldBatchCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fieldSerialNumber = new System.Windows.Forms.TextBox();
            this.tblBatch = new System.Windows.Forms.DataGrid();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Picking By Batch Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 24);
            this.label2.Text = "Batch Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldBatchCode
            // 
            this.fieldBatchCode.Location = new System.Drawing.Point(45, 53);
            this.fieldBatchCode.Name = "fieldBatchCode";
            this.fieldBatchCode.Size = new System.Drawing.Size(150, 23);
            this.fieldBatchCode.TabIndex = 0;
            this.fieldBatchCode.TextChanged += new System.EventHandler(this.fieldBatchCode_TextChanged);
            this.fieldBatchCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldBatchCode_KeyPress);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 20);
            this.label3.Text = "Serial Number";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldSerialNumber
            // 
            this.fieldSerialNumber.Enabled = false;
            this.fieldSerialNumber.Location = new System.Drawing.Point(45, 93);
            this.fieldSerialNumber.Name = "fieldSerialNumber";
            this.fieldSerialNumber.Size = new System.Drawing.Size(150, 23);
            this.fieldSerialNumber.TabIndex = 1;
            this.fieldSerialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldSerialNumber_KeyPress);
            // 
            // tblBatch
            // 
            this.tblBatch.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tblBatch.Location = new System.Drawing.Point(20, 122);
            this.tblBatch.Name = "tblBatch";
            this.tblBatch.RowHeadersVisible = false;
            this.tblBatch.Size = new System.Drawing.Size(200, 150);
            this.tblBatch.TabIndex = 2;
            this.tblBatch.CurrentCellChanged += new System.EventHandler(this.tblBatch_CurrentCellChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(20, 278);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(72, 20);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Location = new System.Drawing.Point(148, 278);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(72, 20);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // formBatchCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tblBatch);
            this.Controls.Add(this.fieldSerialNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fieldBatchCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formBatchCode";
            this.Text = "Picking By Batch Code";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fieldBatchCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fieldSerialNumber;
        private System.Windows.Forms.DataGrid tblBatch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSubmit;
    }
}