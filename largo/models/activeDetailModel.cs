﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace largo.models
{
    class activeDetailModel
    {
        public string status;
        public string param;
        public string message;
        public bool response;
        public string item_name;
        public int qty;
        public List<models.activeDetailResultsModel> results;
    }
}
