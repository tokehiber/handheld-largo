﻿namespace largo
{
    partial class formLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNama = new System.Windows.Forms.Label();
            this.lblDeskripsi = new System.Windows.Forms.Label();
            this.lblUname = new System.Windows.Forms.Label();
            this.fieldUname = new System.Windows.Forms.TextBox();
            this.lblUpass = new System.Windows.Forms.Label();
            this.fieldUpass = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblNama
            // 
            this.lblNama.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold);
            this.lblNama.Location = new System.Drawing.Point(3, 4);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(234, 40);
            this.lblNama.Text = "LARGO";
            this.lblNama.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDeskripsi
            // 
            this.lblDeskripsi.Location = new System.Drawing.Point(3, 48);
            this.lblDeskripsi.Name = "lblDeskripsi";
            this.lblDeskripsi.Size = new System.Drawing.Size(234, 20);
            this.lblDeskripsi.Text = "Warehouse Management System";
            this.lblDeskripsi.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUname
            // 
            this.lblUname.Location = new System.Drawing.Point(3, 96);
            this.lblUname.Name = "lblUname";
            this.lblUname.Size = new System.Drawing.Size(234, 20);
            this.lblUname.Text = "Username";
            this.lblUname.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldUname
            // 
            this.fieldUname.Location = new System.Drawing.Point(58, 113);
            this.fieldUname.Name = "fieldUname";
            this.fieldUname.Size = new System.Drawing.Size(125, 23);
            this.fieldUname.TabIndex = 3;
            this.fieldUname.TextChanged += new System.EventHandler(this.fieldUname_TextChanged);
            // 
            // lblUpass
            // 
            this.lblUpass.Location = new System.Drawing.Point(3, 143);
            this.lblUpass.Name = "lblUpass";
            this.lblUpass.Size = new System.Drawing.Size(234, 20);
            this.lblUpass.Text = "Password";
            this.lblUpass.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldUpass
            // 
            this.fieldUpass.Location = new System.Drawing.Point(58, 160);
            this.fieldUpass.Name = "fieldUpass";
            this.fieldUpass.PasswordChar = '*';
            this.fieldUpass.Size = new System.Drawing.Size(125, 23);
            this.fieldUpass.TabIndex = 5;
            this.fieldUpass.TextChanged += new System.EventHandler(this.fieldUpass_TextChanged);
            // 
            // btnLogin
            // 
            this.btnLogin.Enabled = false;
            this.btnLogin.Location = new System.Drawing.Point(84, 190);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(72, 20);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Login";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // formLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.fieldUpass);
            this.Controls.Add(this.lblUpass);
            this.Controls.Add(this.fieldUname);
            this.Controls.Add(this.lblUname);
            this.Controls.Add(this.lblDeskripsi);
            this.Controls.Add(this.lblNama);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formLogin";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNama;
        private System.Windows.Forms.Label lblDeskripsi;
        private System.Windows.Forms.Label lblUname;
        private System.Windows.Forms.TextBox fieldUname;
        private System.Windows.Forms.Label lblUpass;
        private System.Windows.Forms.TextBox fieldUpass;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.LinkLabel btnClose;
    }
}

