﻿namespace largo
{
    partial class formReceiving
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.lblReceivingCode = new System.Windows.Forms.Label();
            this.fieldReceivingCode = new System.Windows.Forms.TextBox();
            this.tblItemCode = new System.Windows.Forms.DataGrid();
            this.btnScan = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Receiving";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblReceivingCode
            // 
            this.lblReceivingCode.Location = new System.Drawing.Point(3, 50);
            this.lblReceivingCode.Name = "lblReceivingCode";
            this.lblReceivingCode.Size = new System.Drawing.Size(234, 20);
            this.lblReceivingCode.Text = "Receiving Code";
            this.lblReceivingCode.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldReceivingCode
            // 
            this.fieldReceivingCode.Location = new System.Drawing.Point(45, 73);
            this.fieldReceivingCode.Name = "fieldReceivingCode";
            this.fieldReceivingCode.Size = new System.Drawing.Size(150, 23);
            this.fieldReceivingCode.TabIndex = 0;
            this.fieldReceivingCode.TextChanged += new System.EventHandler(this.fieldReceivingCode_TextChanged);
            this.fieldReceivingCode.GotFocus += new System.EventHandler(this.fieldReceivingCode_GotFocus);
            this.fieldReceivingCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldReceivingCode_KeyPress);
            // 
            // tblItemCode
            // 
            this.tblItemCode.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tblItemCode.Location = new System.Drawing.Point(20, 102);
            this.tblItemCode.Name = "tblItemCode";
            this.tblItemCode.RowHeadersVisible = false;
            this.tblItemCode.Size = new System.Drawing.Size(200, 150);
            this.tblItemCode.TabIndex = 1;
            this.tblItemCode.TabStop = false;
            this.tblItemCode.CurrentCellChanged += new System.EventHandler(this.tblItemCode_CurrentCellChanged);
            // 
            // btnScan
            // 
            this.btnScan.Enabled = false;
            this.btnScan.Location = new System.Drawing.Point(20, 258);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(72, 20);
            this.btnScan.TabIndex = 2;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Location = new System.Drawing.Point(148, 258);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(72, 20);
            this.btnSubmit.TabIndex = 3;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // formReceiving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.tblItemCode);
            this.Controls.Add(this.fieldReceivingCode);
            this.Controls.Add(this.lblReceivingCode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formReceiving";
            this.Text = "Manu Receiving";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formReceiving_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label lblReceivingCode;
        private System.Windows.Forms.TextBox fieldReceivingCode;
        private System.Windows.Forms.DataGrid tblItemCode;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Button btnSubmit;
    }
}