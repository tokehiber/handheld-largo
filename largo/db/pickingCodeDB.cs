﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace largo.db
{
    class pickingCodeDB
    {
        public static void insert(string picking, string item)
        {
            string query = "INSERT INTO tbl_picking (pl_name, kd_barang) VALUES ('" + picking + "', '" + item + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void insertLocation(string picking, string barang, string location, int qty)
        {
            string query = "INSERT INTO tbl_picking_helper (pl_name, kd_barang, loc_name, qty) VALUES ('" + picking + "','" + barang + "','" + location + "','" + qty + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void deleteLocation(string picking)
        {
            string query = "DELETE FROM tbl_picking_helper WHERE pl_name = '" + picking + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet getLocation(string picking)
        {
            string query = "SELECT * FROM tbl_picking_helper WHERE pl_name = '" + picking + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getByPicking(string picking, bool picked)
        {
            string query;
            if (picked == true)
            {
                query = "SELECT * FROM tbl_picking WHERE pl_name = '" + picking + "' AND kd_unik IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_picking WHERE pl_name = '" + picking + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getByItem(string picking, string item, bool picked)
        {
            string query;
            if (picked == true)
            {
                query = "SELECT * FROM tbl_picking WHERE pl_name = '" + picking + "' AND kd_barang = '" + item + "' AND kd_unik IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_picking WHERE pl_name = '" + picking + "' AND kd_barang = '" + item + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getBySerialNumber(string serial)
        {
            string query = "SELECT * FROM tbl_picking WHERE kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getGroupByItem(string picking)
        {
            string query = "SELECT * FROM tbl_picking WHERE pl_name = '" + picking + "' GROUP BY kd_barang";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void delete(string picking, string item, string serial)
        {
            string query = "UPDATE tbl_picking SET kd_unik = NULL, user_name_picking = NULL WHERE pl_name = '" + picking + "' AND kd_barang = '" + item + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void update(string picking, string item, string serial)
        {
            string query = "UPDATE tbl_picking SET kd_unik = '" + serial + "', user_name_picking = '" + controllers.userController.getUserInfo("uname") + "' WHERE pl_name = '" + picking + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL AND id = (SELECT min(id) FROM tbl_picking WHERE pl_name = '" + picking + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL)";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string picking, string serial)
        {
            string query = "DELETE FROM tbl_picking WHERE pl_name = '" + picking + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void removeLocation(string picking)
        {
            string query = "DELETE FROM tbl_picking_helper WHERE pl_name = '" + picking + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
