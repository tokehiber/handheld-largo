﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class transferDB
    {
        public static DataSet getByTransferCode(string transfer, bool kosong)
        {
            string query;
            if (kosong)
            {
                query = "SELECT * FROM tbl_transfer WHERE transfer_code = '" + transfer + "' AND loc_name_new IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_transfer WHERE transfer_code = '" + transfer + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet;
                }
            }
        }

        public static DataSet getBySerialNumber(string transfer, string serial)
        {
            string query = "SELECT * FROM tbl_transfer WHERE transfer_code = '" + transfer + "' AND kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet;
                }
            }
        }

        public static void insertPickedSerial(string transfer, string serial, string oldLoc, string uname)
        {
            string query = "INSERT INTO tbl_transfer (transfer_code, kd_unik, loc_name_old, user_name_pick) VALUES('" + transfer + "','" + serial + "','" + oldLoc + "','" + uname + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void updatePickedSerial(string transfer, string serial, string newLoc, string uname, string put_time)
        {
            string query = "UPDATE tbl_transfer SET loc_name_new = '" + newLoc + "', user_name_put = '" + uname + "', put_time = '" + put_time + "' WHERE transfer_code = '" + transfer + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void deletePickedSerial(string transfer, string serial)
        {
            string query = "DELETE FROM tbl_transfer WHERE transfer_code = '" + transfer + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void removePickedSerial(string transfer, string serial)
        {
            string query = "UPDATE tbl_transfer SET loc_name_new = '', user_name_put = '', put_time = '' WHERE transfer_code = '" + transfer + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void deleteByTransferCode(string transfer)
        {
            string query = "DELETE FROM tbl_transfer WHERE transfer_code = '" + transfer + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
