﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace largo.db
{
    class putawayDB
    {
        public static void insertPutawayToLocalDB(string location, string serial)
        {
            string query = "INSERT INTO tbl_putaway (kd_unik, loc_name, user_name_putaway) VALUES ('" + serial + "', '" + location + "', '" + controllers.userController.getUserInfo("uname") + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static int isExist(string serial)
        {
            string query = "SELECT * FROM tbl_putaway WHERE kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet.Tables[0].Rows.Count;
                }
            }
        }

        private static DataSet getDataSet()
        {
            string query = "SELECT kd_unik, loc_name, user_name_putaway FROM tbl_putaway";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet;
                }
            }
        }

        public static DataSet getDataSubmit()
        {
            return getDataSet();
        }

        public static DataTable getDataTable()
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Location", typeof(string));
                DataSet dataSet = new DataSet();
                dataSet = getDataSet();
                int count = dataSet.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string serial = dataSet.Tables[0].Rows[i]["kd_unik"].ToString();
                    string location = dataSet.Tables[0].Rows[i]["loc_name"].ToString();
                    dataTable.Rows.Add(no, serial, location);
                }
                return dataTable;
            }
        }

        public static void removeSerial(string serial)
        {
            string query = "DELETE FROM tbl_putaway WHERE kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static int countPutaway()
        {
            string query = "SELECT * FROM tbl_putaway";
            try
            {
                using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
                {
                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                    {
                        DataSet dataSet = new DataSet();
                        adapter.Fill(dataSet);
                        return dataSet.Tables[0].Rows.Count;
                    }
                }
            }
            catch
            {
                return 0;
            }
        }

        public static void deleteBySerial(string serial)
        {
            string query = "DELETE FROM tbl_putaway WHERE kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private static DataSet get(string item, string serial)
        {
            string query = "SELECT * FROM tbl_putaway WHERE kd_barang = '" + item + "' AND kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getLocation(string location)
        {
            string query = "SELECT * FROM tbl_putaway WHERE loc_name = '" + location + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getLocationNew(string location)
        {
            string query = "SELECT * FROM tbl_putaway WHERE loc_name = '" + location + "' AND loc_name_new IS NOT NULL";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getSerial(string location, string serial)
        {
            string query = "SELECT * FROM tbl_putaway WHERE kd_unik = '" + serial + "' AND loc_name = '" + location + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        private static void insert(string item, string serial, string location)
        {
            string query = "INSERT INTO tbl_putaway (kd_barang, kd_unik, loc_name) VALUES ('" + item + "','" + serial + "','" + location + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void proses(string item, string serial, string location)
        {
            if (get(item, serial).Tables[0].Rows.Count > 0)
            {
                // nothing to do because data exist.
            }
            else
            {
                insert(item, serial, location);
            }
        }

        public static void update(string oldLoc, string serial, string newLoc)
        {
            string query = "UPDATE tbl_putaway SET loc_name_new = '" + newLoc + "', user_name_putaway = '" + controllers.userController.getUserInfo("uname") + "' WHERE loc_name = '" + oldLoc + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string location, string serial)
        {
            string query = "UPDATE tbl_putaway SET loc_name_new = NULL, user_name_putaway = NULL WHERE kd_unik = '" + serial + "' AND loc_name = '" + location + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void delete(string location, string serial)
        {
            string query = "DELETE FROM tbl_putaway WHERE kd_unik = '" + serial + "' AND loc_name = '" + location + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
