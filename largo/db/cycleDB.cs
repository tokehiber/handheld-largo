﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class cycleDB
    {
        public static DataSet getByCycle(string cycle, bool cycled)
        {
            string query;
            if (cycled == true)
            {
                query = "SELECT * FROM tbl_cycle WHERE cc_code = '" + cycle + "' AND st_cc = 'YES'";
            }
            else
            {
                query = "SELECT * FROM tbl_cycle WHERE cc_code = '" + cycle + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void insert(string cycle, string item, string serial, string location)
        {
            string query = "INSERT INTO tbl_cycle (cc_code, kd_barang, kd_unik, loc_name) VALUES ('" + cycle + "','" + item + "','" + serial + "','" + location + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet getBySerial(string cycle, string serial)
        {
            string query = "SELECT * FROM tbl_cycle WHERE cc_code = '" + cycle + "' AND kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void update(string cycle, string serial, string status)
        {
            string query = updateQuery(cycle, serial, status);
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private static string updateQuery(string cycle, string serial, string status)
        {
            switch (status)
            {
                case "cycle":
                    return "UPDATE tbl_cycle SET user_name_cc = '" + controllers.userController.getUserInfo("uname") + "', st_cc = 'YES' WHERE cc_code = '" + cycle + "' AND kd_unik = '" + serial + "'";
                    break;
                case "remove":
                    return "UPDATE tbl_cycle SET user_name_cc = NULL, st_cc = NULL WHERE cc_code = '" + cycle + "' AND kd_unik = '" + serial + "'";
                    break;
                default:
                    return "";
                    break;
            }
        }

        public static DataSet getReason(string cycle)
        {
            string query = "SELECT * FROM tbl_reason WHERE reason_code = '" + cycle + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void removeReason(string cycle)
        {
            string query = "DELETE FROM tbl_reason WHERE reason_code = '" + cycle + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string cycle, string serial)
        {
            string query = "DELETE FROM tbl_cycle WHERE cc_code = '" + cycle + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void delete(string cycle)
        {
            string query = "DELETE FROM tbl_cycle WHERE cc_code = '" + cycle + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
