﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class receivingDB
    {
        public static int countByReceivingCode(string receivingCode)
        {
            string query = "SELECT id FROM tbl_receiving WHERE kd_receiving = '" + receivingCode + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataTable = new DataSet();
                    adapter.Fill(dataTable);
                    return dataTable.Tables[0].Rows.Count;
                }
            }
        }

        public static int countByItemCode(string receivingCode, string itemCode)
        {
            string query = "SELECT id FROM tbl_receiving WHERE kd_receiving = '" + receivingCode + "' AND kd_barang = '" + itemCode + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataTable = new DataSet();
                    adapter.Fill(dataTable);
                    return dataTable.Tables[0].Rows.Count;
                }
            }
        }

        public static int countBySerialNumber(string receivingCode, string itemCode)
        {
            string query = "SELECT id FROM tbl_receiving WHERE kd_receiving = '" + receivingCode + "' AND kd_barang = '" + itemCode + "' AND kd_unik IS NOT NULL";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataTable = new DataSet();
                    adapter.Fill(dataTable);
                    return dataTable.Tables[0].Rows.Count;
                }
            }
        }

        public static int countBySerialOnReceiving(string receiving)
        {
            string query = "SELECT id FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_unik IS NOT NULL";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet.Tables[0].Rows.Count;
                }
            }
        }

        public static void insertReceivingToLocalDB(string receiving, string barang, string exp)
        {
            string query = "INSERT INTO tbl_receiving (kd_receiving, kd_barang, has_expdate) VALUES ('" + receiving + "', '" + barang + "', '" + exp + "')";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private static DataSet getItemByReceivingCode(string receivingCode)
        {
            string query = "SELECT kd_barang FROM tbl_receiving WHERE kd_receiving = '" + receivingCode + "' GROUP BY kd_barang";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataTable = new DataSet();
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }

        public static DataTable getTableByReceivingCode(string receivingCode)
        {
            using (DataTable dataTable = new DataTable())
            {
                DataSet listItemCode = new DataSet();
                listItemCode = getItemByReceivingCode(receivingCode);
                int count = listItemCode.Tables[0].Rows.Count;
                int no = 0;
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("QTY", typeof(string));
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string item = listItemCode.Tables[0].Rows[i]["kd_barang"].ToString();
                    int N = countByItemCode(receivingCode, item);
                    int n = countBySerialNumber(receivingCode, item);
                    string qty = n.ToString() + "/" + N.ToString();
                    dataTable.Rows.Add(no, item, qty);
                }
                return dataTable;
            }
        }

        public static DataSet getReceiving(string receivingCode)
        {
            string query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receivingCode + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet;
                }
            }
        }

        public static void deleteBySerialNumber(string serial)
        {
            string query = "DELETE FROM tbl_receiving WHERE kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void deleteByReceivingCode(string receiving)
        {
            string query = "DELETE FROM tbl_receiving WHERE kd_receiving = '" + receiving + "'";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet get(string receiving, bool serialed)
        {
            string query;
            if (serialed)
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_unik IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getByItem(string receiving, string item, bool serialed)
        {
            string query;
            if (serialed)
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet group(string receiving)
        {
            string query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' GROUP BY kd_barang";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void insert(string receiving, string item, int st_batch, int has_expdate)
        {
            string query = "INSERT INTO tbl_receiving (kd_receiving, kd_barang, st_batch, has_expdate) VALUES ('" + receiving + "','" + item + "','" + st_batch + "','" + has_expdate + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string receiving, string serial)
        {
            string query = "DELETE FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet getReason(string receiving)
        {
            string query = "SELECT * FROM tbl_reason WHERE reason_code = '" + receiving + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void delete(string receiving)
        {
            string query = "DELETE FROM tbl_receiving WHERE kd_receiving = '" + receiving + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
