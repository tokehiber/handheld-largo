﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace largo.db
{
    class localDB
    {
        public static string getDataSource()
        {
            string fullPathName = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
            string getPath = Path.GetDirectoryName(fullPathName);
            return @"Data Source = " + getPath + @"\db\db_largo.s3db";
        }
    }
}
