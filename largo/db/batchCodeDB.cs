﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace largo.db
{
    class batchCodeDB
    {
        public static void insert(string batch, string barang, string serial, string location, string picked)
        {
            string query = "INSERT INTO tbl_picking (kd_barang, kd_unik, kd_batch, loc_name, st_pick) VALUES ('" + barang + "','" + serial + "','" + batch + "','" + location + "','" + picked + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void reput(string batch, string serial)
        {
            string query = "UPDATE tbl_picking SET st_pick = 'NO' WHERE kd_batch = '" + batch + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet getByBatchCode(string batch, bool picked)
        {
            string query;
            if (picked)
            {
                query = "SELECT * FROM tbl_picking WHERE kd_batch = '" + batch + "' AND st_pick = 'YES'";
            }
            else
            {
                query = "SELECT * FROM tbl_picking WHERE kd_batch = '" + batch + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet get(string batch, string serial)
        {
            string query = "SELECT * FROM tbl_picking WHERE kd_batch = '" + batch + "' AND kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void update(string batch, string serial)
        {
            string query = "UPDATE tbl_picking SET st_pick = 'YES' WHERE kd_batch = '" + batch + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string batch, string serial)
        {
            string query = "DELETE FROM tbl_picking WHERE kd_batch = '" + batch + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
