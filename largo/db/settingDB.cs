﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;

namespace largo.db
{
    class settingDB
    {
        public static void truncateLocation()
        {
            string query = "DELETE FROM tbl_location";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void insertLocation(string loc, string desc)
        {
            string query = "INSERT INTO tbl_location (loc_name, loc_desc) VALUES('" + loc + "','" + desc + "')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private static DataSet getDataTableLocation()
        {
            string query = "SELECT loc_name, loc_desc FROM tbl_location";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet;
                }
            }
        }

        public static DataTable getTableLocation()
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Loc Name", typeof(string));
                dataTable.Columns.Add("Loc Desc", typeof(string));

                DataSet dataSet = new DataSet();
                dataSet = getDataTableLocation();
                int count = dataSet.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string loc_name = dataSet.Tables[0].Rows[i]["loc_name"].ToString();
                    string loc_desc = dataSet.Tables[0].Rows[i]["loc_desc"].ToString();
                    dataTable.Rows.Add(no, loc_name, loc_desc);
                }
                return dataTable;
            }
        }
    }
}
