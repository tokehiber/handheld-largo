﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class serialNumberDB
    {
        public static bool itemCodeHasExpDate(string item)
        {
            string query = "SELECT has_expdate FROM tbl_receiving WHERE kd_barang = '" + item + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataTable = new DataSet();
                    adapter.Fill(dataTable);
                    if (dataTable.Tables[0].Rows[0]["has_expdate"].ToString() == "1")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
        }

        public static DataTable getTableByScannedSerialNumber(string receiving, string item)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Exp Date", typeof(string));

                string query = "SELECT kd_unik, tgl_exp FROM tbl_receiving WHERE kd_unik IS NOT NULL AND kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "'";
                using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
                {
                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                    {
                        DataSet dataSet = new DataSet();
                        adapter.Fill(dataSet);
                        int count = dataSet.Tables[0].Rows.Count;
                        int no = 0;
                        for (int i = 0; i < count; i++)
                        {
                            no += 1;
                            string serial = dataSet.Tables[0].Rows[i]["kd_unik"].ToString();
                            string date = dataSet.Tables[0].Rows[i]["tgl_exp"].ToString();
                            dataTable.Rows.Add(no, serial, date);
                        }
                    }
                }
                return dataTable;
            }
        }

        public static int countBySerialNumber(string serial)
        {
            string query = "SELECT id FROM tbl_receiving WHERE kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet dataSet = new DataSet();
                    adapter.Fill(dataSet);
                    return dataSet.Tables[0].Rows.Count;
                }
            }
        }

        public static void insertSerialByReceivingAndItem(string receiving, string item, string serial, bool isExp, string date)
        {
            string query;
            if (isExp == true)
            {
                query = "UPDATE tbl_receiving SET kd_unik = '" + serial + "', tgl_exp = '" + date + "', tgl_in = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', user_name_receiving = '" + controllers.userController.getUserInfo("uname") + "' WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL AND id = (SELECT min(id) FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL)";
            }
            else
            {
                query = "UPDATE tbl_receiving SET kd_unik = '" + serial + "', tgl_in = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', user_name_receiving = '" + controllers.userController.getUserInfo("uname") + "' WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND id = (SELECT min(id) FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL)";
            }

            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void deleteSerialNumber(string serial)
        {
            string query = "UPDATE tbl_receiving SET kd_unik = NULL, tgl_exp = NULL, user_name_receiving = NULL WHERE kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet get(string receiving, string item, bool serialed)
        {
            string query;
            if (serialed)
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NOT NULL";
            }
            else
            {
                query = "SELECT * FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static DataSet getBySerial(string serial)
        {
            string query = "SELECT * FROM tbl_receiving WHERE kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void remove(string receiving, string item, string serial)
        {
            string query = "UPDATE tbl_receiving SET kd_unik = NULL, kd_batch = NULL, tgl_exp = NULL, tgl_in = NULL, user_name_receiving = NULL WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void insert(string receiving, string item, string serial, string batch, string expdate)
        {
            string query = "UPDATE tbl_receiving SET kd_unik = '" + serial + "', kd_batch = '" + batch + "', tgl_exp = '" + expdate + "', tgl_in = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', user_name_receiving = '" + controllers.userController.getUserInfo("uname") + "' WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL AND id = (SELECT MIN(id) FROM tbl_receiving WHERE kd_receiving = '" + receiving + "' AND kd_barang = '" + item + "' AND kd_unik IS NULL)";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
