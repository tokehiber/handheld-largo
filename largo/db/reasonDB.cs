﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class reasonDB
    {
        public static void delete(string code)
        {
            string query = "DELETE FROM tbl_reason WHERE reason_code = '" + code + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public void addReason(string reason, string destination, string code)
        {
            switch (destination)
            {
                case "receiving":
                    this.reasonReceiving(reason, code);
                    break;
                case "cycle":
                    this.reasonCycle(reason, code);
                    break;
                default:
                    break;
            }
        }

        private void reasonReceiving(string reason, string code)
        {
            string query = "INSERT INTO tbl_reason (reason_code, reason_desc) VALUES ('" + code + "','" + reason + "')";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private void reasonCycle(string reason, string code)
        {
            string query = "INSERT INTO tbl_reason (reason_code, reason_desc) VALUES ('" + code + "','" + reason + "')";
            SQLiteConnection koneksi = new SQLiteConnection(localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
