﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace largo.db
{
    class shippingDB
    {
        public static DataSet getByShipping(string shipping, bool shippied)
        {
            string query;
            if (shippied == true)
            {
                query = "SELECT * FROM tbl_shipping WHERE kd_shipping = '" + shipping + "' AND st_ship = 'YES'";
            }
            else
            {
                query = "SELECT * FROM tbl_shipping WHERE kd_shipping = '" + shipping + "'";
            }
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void insert(string shipping, string item, string serial, string location)
        {
            string query = "INSERT INTO tbl_shipping (kd_shipping, kd_barang, kd_unik, loc_name, st_ship) VALUES ('" + shipping + "','" + item + "','" + serial + "','" + location + "','NO')";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static DataSet getBySerial(string serial)
        {
            string query = "SELECT * FROM tbl_shipping WHERE kd_unik = '" + serial + "'";
            using (SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource()))
            {
                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, koneksi))
                {
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    return data;
                }
            }
        }

        public static void update(string serial, string status)
        {
            string query = updateQuery(serial, status);
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        private static string updateQuery(string serial, string status)
        {
            switch (status)
            {
                case "ship":
                    return "UPDATE tbl_shipping SET user_name_shipping = '" + controllers.userController.getUserInfo("uname") + "', st_ship = 'YES' WHERE kd_unik = '" + serial + "'";
                    break;
                case "remove":
                    return "UPDATE tbl_shipping SET user_name_shipping = NULL, st_ship = 'NO' WHERE kd_unik = '" + serial + "'";
                    break;
                default:
                    return "";
                    break;
            }
        }

        public static void delete(string shipping)
        {
            string query = "DELETE FROM tbl_shipping WHERE kd_shipping = '" + shipping + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }

        public static void remove(string shipping, string serial)
        {
            string query = "DELETE FROM tbl_shipping WHERE kd_shipping = '" + shipping + "' AND kd_unik = '" + serial + "'";
            SQLiteConnection koneksi = new SQLiteConnection(db.localDB.getDataSource());
            SQLiteCommand cmd = new SQLiteCommand(query, koneksi);
            koneksi.Open();
            cmd.ExecuteNonQuery();
            koneksi.Close();
        }
    }
}
