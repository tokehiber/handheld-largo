﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formActiveStock : Form
    {
        private formActiveDetail menuActiveDetail;

        public formActiveStock()
        {
            InitializeComponent();
            menuActiveDetail = new formActiveDetail();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fieldSerialNumber_TextChanged(object sender, EventArgs e)
        {
            lblItemCode.Text = "-";
            lblLocation.Text = "-";
            lblDateIn.Text = "-";
            lblExpireDate.Text = "-";
            lblName.Text = "";
            btnDetail.Visible = false;
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string serial = fieldSerialNumber.Text;
                    string url = controllers.serverController.getServerAddress() + apiLists.activeStock + serial + "/";
                    apiAgent.get(url, parseJsonActiveStockResponse, true);
                }
            }
        }

        private void parseJsonActiveStockResponse(string response)
        {
            models.activeStockModel jsonResponse = JsonConvert.DeserializeObject<models.activeStockModel>(response);
            if (jsonResponse.response)
            {
                lblName.Text = jsonResponse.item_name;
                lblItemCode.Text = jsonResponse.item_code;
                lblLocation.Text = jsonResponse.location;
                lblDateIn.Text = jsonResponse.date_in;
                lblExpireDate.Text = jsonResponse.exp_date;
                btnDetail.Visible = true;
                menuActiveDetail.setItemCode(jsonResponse.item_code);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldSerialNumber.Text = "";
            }
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            menuActiveDetail.Show();
        }
    }
}