﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formSerialNumber : Form
    {
        private string receivingCode;
        private string itemCode;
        private string serialNumber;
        private bool stBatch;
        private bool hasExpDate;
        private string batchCode;
        private string expDate;

        public formSerialNumber()
        {
            InitializeComponent();
        }

        private void switcher(bool batch, bool exp)
        {
            DataSet data = db.serialNumberDB.get(receivingCode, itemCode, true);
            tblSerialNumber.DataSource = controllers.serialNumberController.getTableByDataItem(data);
            if (batch == true && exp == true)
            {
                lblBatchCode.Visible = true;
                fieldBatchCode.Enabled = true;
                fieldBatchCode.Visible = true;
                lblExpDate.Visible = true;
                fieldExpireDate.Enabled = true;
                fieldExpireDate.Visible = true;
                tblSerialNumber.Height = 115;
                tblSerialNumber.Location = new Point(20, 156);
                lblBatchCode.Location = new Point(3, 74);
                fieldBatchCode.Location = new Point(45, 89);
                lblExpDate.Location = new Point(3, 111);
                fieldExpireDate.Location = new Point(45, 126);
            }
            else if (batch == true && exp == false)
            {
                lblBatchCode.Visible = true;
                fieldBatchCode.Enabled = true;
                fieldBatchCode.Visible = true;
                lblExpDate.Visible = false;
                fieldExpireDate.Enabled = false;
                fieldExpireDate.Visible = false;
                tblSerialNumber.Height = 153;
                tblSerialNumber.Location = new Point(20, 118);
                lblBatchCode.Location = new Point(3, 74);
                fieldBatchCode.Location = new Point(45, 89);
            }
            else if (batch == false && exp == true)
            {
                lblExpDate.Visible = true;
                fieldExpireDate.Enabled = true;
                fieldExpireDate.Visible = true;
                lblBatchCode.Visible = false;
                fieldBatchCode.Enabled = false;
                fieldBatchCode.Visible = false;
                tblSerialNumber.Height = 153;
                tblSerialNumber.Location = new Point(20, 118);
                lblExpDate.Location = new Point(3, 74);
                fieldExpireDate.Location = new Point(45, 89);
            }
            else
            {
                lblBatchCode.Visible = false;
                fieldBatchCode.Enabled = false;
                fieldBatchCode.Visible = false;
                lblExpDate.Visible = false;
                fieldExpireDate.Enabled = false;
                fieldExpireDate.Visible = false;
                tblSerialNumber.Height = 190;
                tblSerialNumber.Location = new Point(20, 81);
            }
        }

        public void setFromReceiving(string receiving, string item)
        {
            receivingCode = receiving;
            itemCode = item;
            DataSet data = db.serialNumberDB.get(receiving, item, false);
            if (data.Tables[0].Rows[0]["st_batch"].ToString() == "1")
            {
                stBatch = true;
            }
            else
            {
                stBatch = false;
            }
            if (data.Tables[0].Rows[0]["has_expdate"].ToString() == "1")
            {
                hasExpDate = true;
            }
            else
            {
                hasExpDate = false;
            }
        }

        private void tblSerialNumber_CurrentCellChanged(object sender, EventArgs e)
        {
            tblSerialNumber.Select(tblSerialNumber.CurrentRowIndex);
            serialNumber = tblSerialNumber[tblSerialNumber.CurrentCell.RowNumber, 1].ToString();
            btnDelete.Enabled = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure to exit?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                this.Hide();
            }
            else
            {
                fieldSerialNumber.Focus();
            }
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    int n = db.serialNumberDB.get(receivingCode, itemCode, true).Tables[0].Rows.Count;
                    int N = db.serialNumberDB.get(receivingCode, itemCode, false).Tables[0].Rows.Count;
                    if (n > 0 && n == N)
                    {
                        MessageBox.Show("It should be enough.\nScanned item: " + n + " of " + N + ".", receivingCode + ": " + itemCode);
                        btnDelete.Enabled = false;
                        btnDone.Enabled = true;
                        btnDone.Focus();
                    }
                    else
                    {
                        if (db.serialNumberDB.getBySerial(fieldSerialNumber.Text).Tables[0].Rows.Count > 0)
                        {
                            MessageBox.Show(fieldSerialNumber.Text + " is not available.", "Error: 401");
                        }
                        else
                        {
                            string url = controllers.serverController.getServerAddress() + apiLists.isValidSNReceiving + fieldSerialNumber.Text + "/";
                            apiAgent.get(url, parseSerialNumberResponse, true);
                        }
                        btnDelete.Enabled = false;
                    }
                    fieldSerialNumber.Text = "";
                }
            }
        }

        private void parseSerialNumberResponse(string response)
        {
            models.serialNumberModel jsonResponse = JsonConvert.DeserializeObject<models.serialNumberModel>(response);
            if (jsonResponse.response)
            {
                if (fieldBatchCode.Enabled == true)
                {
                    batchCode = fieldBatchCode.Text;
                }
                else
                {
                    batchCode = null;
                }

                if (fieldExpireDate.Enabled == true)
                {
                    expDate = fieldExpireDate.Value.ToString("yyyy-MM-dd");
                }
                else
                {
                    expDate = null;
                }

                db.serialNumberDB.insert(receivingCode, itemCode, jsonResponse.param, batchCode, expDate);
                this.switcher(stBatch, hasExpDate);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
            fieldSerialNumber.Focus();
        }

        private void formSerialNumber_Activated(object sender, EventArgs e)
        {
            lblTitle.Text = receivingCode + ": " + itemCode;
            fieldSerialNumber.Focus();
            this.switcher(stBatch, hasExpDate);
            btnDone.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure to remove " + serialNumber + " from " + receivingCode + ": " + itemCode + "?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                db.serialNumberDB.remove(receivingCode, itemCode, serialNumber);
                this.switcher(stBatch, hasExpDate);
                btnDelete.Enabled = false;
                fieldSerialNumber.Focus();
            }
            else
            {
                fieldSerialNumber.Focus();
            }
            btnDelete.Enabled = false;
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            int n = db.serialNumberDB.get(receivingCode, itemCode, true).Tables[0].Rows.Count;
            int N = db.serialNumberDB.get(receivingCode, itemCode, false).Tables[0].Rows.Count;
            if (n < N)
            {
                MessageBox.Show("You have " + Convert.ToString(N-n) + " item(s) left.\nScanned item: " + n + " of " + N + ".", "Undone work!");
                fieldSerialNumber.Focus();
            }
            else
            {
                this.Hide();
            }
        }

        private void tblSerialNumber1_CurrentCellChanged(object sender, EventArgs e)
        {
            
        }

        private void tblSerialNumber2_CurrentCellChanged(object sender, EventArgs e)
        {
            
        }

        private void fieldSerialNumber_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }

        private void fieldBatchCode_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }

        private void fieldExpireDate_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }

        private void fieldExpireDate1_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }
    }
}