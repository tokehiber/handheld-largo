﻿namespace largo
{
    partial class formReason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSubmit = new System.Windows.Forms.Button();
            this.fieldReason = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(98, 42);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(72, 20);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // fieldReason
            // 
            this.fieldReason.Location = new System.Drawing.Point(3, 13);
            this.fieldReason.Name = "fieldReason";
            this.fieldReason.Size = new System.Drawing.Size(167, 23);
            this.fieldReason.TabIndex = 3;
            // 
            // formReason
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(173, 75);
            this.ControlBox = false;
            this.Controls.Add(this.fieldReason);
            this.Controls.Add(this.btnSubmit);
            this.Location = new System.Drawing.Point(34, 89);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formReason";
            this.Text = "Submit Reason";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formReason_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox fieldReason;
    }
}