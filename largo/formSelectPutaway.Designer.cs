﻿namespace largo
{
    partial class formSelectPutaway
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.putwayInbound = new System.Windows.Forms.CheckBox();
            this.putwayOutbound = new System.Windows.Forms.CheckBox();
            this.putwayIntransfer = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 20);
            this.label1.Text = "Select Location";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // putwayInbound
            // 
            this.putwayInbound.Location = new System.Drawing.Point(45, 124);
            this.putwayInbound.Name = "putwayInbound";
            this.putwayInbound.Size = new System.Drawing.Size(150, 20);
            this.putwayInbound.TabIndex = 3;
            this.putwayInbound.Text = "INBOUND";
            this.putwayInbound.CheckStateChanged += new System.EventHandler(this.putwayInbound_CheckStateChanged);
            // 
            // putwayOutbound
            // 
            this.putwayOutbound.Location = new System.Drawing.Point(45, 150);
            this.putwayOutbound.Name = "putwayOutbound";
            this.putwayOutbound.Size = new System.Drawing.Size(150, 20);
            this.putwayOutbound.TabIndex = 4;
            this.putwayOutbound.Text = "OUTBOUND";
            this.putwayOutbound.CheckStateChanged += new System.EventHandler(this.putwayOutbound_CheckStateChanged);
            // 
            // putwayIntransfer
            // 
            this.putwayIntransfer.Location = new System.Drawing.Point(45, 176);
            this.putwayIntransfer.Name = "putwayIntransfer";
            this.putwayIntransfer.Size = new System.Drawing.Size(150, 20);
            this.putwayIntransfer.TabIndex = 5;
            this.putwayIntransfer.Text = "IN-TRANSFER";
            this.putwayIntransfer.CheckStateChanged += new System.EventHandler(this.putwayIntransfer_CheckStateChanged);
            // 
            // formSelectPutaway
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.putwayIntransfer);
            this.Controls.Add(this.putwayOutbound);
            this.Controls.Add(this.putwayInbound);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formSelectPutaway";
            this.Text = "Select Putaway";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formSelectPutaway_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.CheckBox putwayInbound;
        private System.Windows.Forms.CheckBox putwayOutbound;
        private System.Windows.Forms.CheckBox putwayIntransfer;
    }
}