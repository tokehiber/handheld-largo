﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formBatchCode : Form
    {
        private string batchCode;
        private string serialNumber;

        public formBatchCode()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fieldBatchCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldBatchCode.Text))
                {
                    string batch = fieldBatchCode.Text;
                    DataSet data = db.batchCodeDB.getByBatchCode(batch, false);
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        batchCode = batch;
                        tblBatch.DataSource = controllers.batchCodeController.getTableByDataBatch(data);
                        fieldSerialNumber.Enabled = true;
                        fieldSerialNumber.Focus();
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.getBatchCode + batch + "/";
                        apiAgent.get(url, parseRetrieveBatchResponse, true);
                    }
                }
            }
        }

        private void parseRetrieveBatchResponse(string response)
        {
            models.batchCodeModel jsonResponse = JsonConvert.DeserializeObject<models.batchCodeModel>(response);
            if (jsonResponse.response)
            {
                batchCode = fieldBatchCode.Text;
                foreach (var asdf in jsonResponse.results)
                {
                    db.batchCodeDB.insert(asdf.kd_batch, asdf.kd_barang, asdf.kd_unik, asdf.loc_name, "NO");
                }
                DataSet data = db.batchCodeDB.getByBatchCode(jsonResponse.param, false);
                tblBatch.DataSource = controllers.batchCodeController.getTableByDataBatch(data);
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldBatchCode.Text = "";
            }
        }

        private void fieldBatchCode_TextChanged(object sender, EventArgs e)
        {
            tblBatch.DataSource = null;
            tblBatch.Refresh();
            btnDelete.Enabled = false;
            btnSubmit.Enabled = false;
            fieldSerialNumber.Enabled = false;
        }

        private void tblBatch_CurrentCellChanged(object sender, EventArgs e)
        {
            tblBatch.Select(tblBatch.CurrentRowIndex);
            serialNumber = tblBatch[tblBatch.CurrentCell.RowNumber, 2].ToString();
            string picked = tblBatch[tblBatch.CurrentCell.RowNumber, 4].ToString();
            if (picked == "YES")
            {
                btnDelete.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                db.batchCodeDB.reput(batchCode, serialNumber);
                DataSet data = db.batchCodeDB.getByBatchCode(batchCode, false);
                tblBatch.DataSource = controllers.batchCodeController.getTableByDataBatch(data);
            }
            fieldSerialNumber.Focus();
            btnDelete.Enabled = false;
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string serial = fieldSerialNumber.Text;
                    DataSet data = db.batchCodeDB.get(batchCode, serial);
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        db.batchCodeDB.update(batchCode, serial);
                        tblBatch.DataSource = controllers.batchCodeController.getTableByDataBatch(db.batchCodeDB.getByBatchCode(batchCode, false));
                        DataSet n = db.batchCodeDB.getByBatchCode(batchCode, true);
                        DataSet N = db.batchCodeDB.getByBatchCode(batchCode, false);
                        if (n.Tables[0].Rows.Count == N.Tables[0].Rows.Count && n.Tables[0].Rows.Count > 0)
                        {
                            btnSubmit.Enabled = true;
                        }
                        else
                        {
                            btnSubmit.Enabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid serial number.", "Warning!");
                    }
                    fieldSerialNumber.Text = "";
                    fieldSerialNumber.Focus();
                }
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string urlSet = controllers.serverController.getServerAddress() + apiLists.setPicking;
            string urlLock = controllers.serverController.getServerAddress() + apiLists.lockPicking;
            DataSet data = db.batchCodeDB.getByBatchCode(batchCode, true);
            int count = data.Tables[0].Rows.Count;
            for (int i = 0; i < count; i++)
            {
                string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                string uname = data.Tables[0].Rows[i]["user_name_picking"].ToString();
                string status = data.Tables[0].Rows[i]["st_pick"].ToString();
                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                httpParams.Add(new KeyValuePair<string, string>("batch_code", batchCode));
                httpParams.Add(new KeyValuePair<string, string>("item_code", item));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("uname_pick", uname));
                if (status == "YES")
                {
                    apiAgent.post(urlSet, httpParams, parseJsonSubmitBySerialResponse, true);
                }
                else
                {
                    db.batchCodeDB.remove(batchCode, serial);
                }
            }
            List<KeyValuePair<string, string>> httpParamsLock = new List<KeyValuePair<string, string>>();
            httpParamsLock.Add(new KeyValuePair<string, string>("picking_code", batchCode));
            httpParamsLock.Add(new KeyValuePair<string, string>("picking_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            apiAgent.post(urlLock, httpParamsLock, parseJsonSubmitLockResponse, true);
        }

        private void parseJsonSubmitBySerialResponse(string response)
        {
            models.batchCodeModel jsonResponse = JsonConvert.DeserializeObject<models.batchCodeModel>(response);
            if (jsonResponse.response)
            {
                db.batchCodeDB.remove(batchCode, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseJsonSubmitLockResponse(string response)
        {
            models.batchCodeModel jsonResponse = JsonConvert.DeserializeObject<models.batchCodeModel>(response);
            if (jsonResponse.response)
            {
                MessageBox.Show(jsonResponse.message, jsonResponse.param);
                fieldBatchCode.Text = "";
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }
    }
}