﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;

namespace largo
{
    class apiAgent
    {
        public static void post(string url, List<KeyValuePair<string, string>> httpParams, Action<string> callbackFunction, bool includeHeader)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);

                if (includeHeader)
                {
                    WebHeaderCollection webHeaderCollection = new WebHeaderCollection();
                    string uname = controllers.userController.getUserInfo("uname");
                    string sessionCode = controllers.userController.getUserInfo("session_code");
                    webHeaderCollection.Add("USER", uname);
                    webHeaderCollection.Add("Authorization", sessionCode);
                    request.Headers = webHeaderCollection;
                }

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Timeout = 20000;
                string contentParams = "";

                foreach (KeyValuePair<string, string> pair in httpParams)
                {
                    contentParams += (contentParams.Length > 0 ? "&" : "") + pair.Key + "=" + pair.Value;
                }

                request.ContentLength = contentParams.Length;
                Stream requestStream = request.GetRequestStream();
                StreamWriter requestStreamWriter = new StreamWriter(requestStream);
                requestStreamWriter.Write(contentParams);
                requestStreamWriter.Close();
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader responseStreamReader = new StreamReader(responseStream);

                string responseText = responseStreamReader.ReadToEnd();
                responseStreamReader.Close();
                responseStream.Close();

                callbackFunction(responseText);
            }
            catch (TimeoutException exTimeOut)
            {
                MessageBox.Show(exTimeOut.Message, "Failed to connect");
                DialogResult konfirmasi = MessageBox.Show("Change server address?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formServer.getInstance().Show();
                }
            }
            catch (WebException exWeb)
            {
                MessageBox.Show(exWeb.Message, "Failed to connect");
                DialogResult konfirmasi = MessageBox.Show("Change server address?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formServer.getInstance().Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Failed to connect");
            }
        }

        public static void get(string url, Action<string> callbackFunction, bool includeHeader)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);

                if (includeHeader)
                {
                    WebHeaderCollection webHeaderCollection = new WebHeaderCollection();
                    string uname = controllers.userController.getUserInfo("uname");
                    string sessionCode = controllers.userController.getUserInfo("session_code");
                    webHeaderCollection.Add("USER", uname);
                    webHeaderCollection.Add("Authorization", sessionCode);
                    request.Headers = webHeaderCollection;
                }

                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Timeout = 20000;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader responseStreamReader = new StreamReader(responseStream);

                string responseText = responseStreamReader.ReadToEnd();
                responseStreamReader.Close();
                responseStream.Close();

                callbackFunction(responseText);
            }
            catch (TimeoutException exTimeOut)
            {
                MessageBox.Show(exTimeOut.Message, "Failed to connect");
                DialogResult konfirmasi = MessageBox.Show("Change server address?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formServer.getInstance().Show();
                }
            }
            catch (WebException exWeb)
            {
                MessageBox.Show(exWeb.Message, "Failed to connect");
                DialogResult konfirmasi = MessageBox.Show("Change server address?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formServer.getInstance().Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Failed to connect");
            }
        }

    }
}
