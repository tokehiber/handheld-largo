﻿namespace largo
{
    partial class formCycleCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.fieldCycleCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fieldSerialNumber = new System.Windows.Forms.TextBox();
            this.tblCycle = new System.Windows.Forms.DataGrid();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Cycle Count";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 20);
            this.label2.Text = "Cycle Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldCycleCode
            // 
            this.fieldCycleCode.Location = new System.Drawing.Point(45, 52);
            this.fieldCycleCode.Name = "fieldCycleCode";
            this.fieldCycleCode.Size = new System.Drawing.Size(150, 23);
            this.fieldCycleCode.TabIndex = 0;
            this.fieldCycleCode.TextChanged += new System.EventHandler(this.fieldCycleCode_TextChanged);
            this.fieldCycleCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldCycleCode_KeyPress);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 20);
            this.label3.Text = "Serial Number";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldSerialNumber
            // 
            this.fieldSerialNumber.Enabled = false;
            this.fieldSerialNumber.Location = new System.Drawing.Point(45, 92);
            this.fieldSerialNumber.Name = "fieldSerialNumber";
            this.fieldSerialNumber.Size = new System.Drawing.Size(150, 23);
            this.fieldSerialNumber.TabIndex = 1;
            this.fieldSerialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.fieldSerialNumber_KeyPress);
            // 
            // tblCycle
            // 
            this.tblCycle.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tblCycle.Location = new System.Drawing.Point(20, 122);
            this.tblCycle.Name = "tblCycle";
            this.tblCycle.RowHeadersVisible = false;
            this.tblCycle.Size = new System.Drawing.Size(200, 150);
            this.tblCycle.TabIndex = 2;
            this.tblCycle.CurrentCellChanged += new System.EventHandler(this.tblCycle_CurrentCellChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(20, 278);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(72, 20);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Enabled = false;
            this.btnSubmit.Location = new System.Drawing.Point(148, 278);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(72, 20);
            this.btnSubmit.TabIndex = 4;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // formCycleCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tblCycle);
            this.Controls.Add(this.fieldSerialNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fieldCycleCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formCycleCount";
            this.Text = "Cycle Count";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fieldCycleCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fieldSerialNumber;
        private System.Windows.Forms.DataGrid tblCycle;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSubmit;
    }
}