﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formTransfer : Form
    {
        private formProsesTransfer menuProsesTransfer;
        private string transferCode;
        private string serialNumber;
        private string locationName;

        public formTransfer()
        {
            InitializeComponent();
            menuProsesTransfer = new formProsesTransfer();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            fieldTransferCode.Text = "";
            fieldSerialNumber.Text = "";
            tblTransfer.DataSource = null;
            tblTransfer.Refresh();
            btnDelete.Enabled = false;
            btnProcess.Enabled = false;
            this.Hide();
        }

        private void fieldTransferCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldTransferCode.Text))
                {
                    string transferCode = fieldTransferCode.Text;
                    DataSet dataSet = db.transferDB.getByTransferCode(transferCode, false);
                    int count = dataSet.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
                        fieldSerialNumber.Enabled = true;
                        fieldSerialNumber.Focus();
                        btnProcess.Enabled = true;
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.isTransfered + transferCode + "/";
                        apiAgent.get(url, parseIsTransferedResponse, true);
                    }
                }
            }
        }

        private void parseIsTransferedResponse(string response)
        {
            models.transferModel jsonResponse = JsonConvert.DeserializeObject<models.transferModel>(response);
            if (jsonResponse.response)
            {
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string serial = fieldSerialNumber.Text;
                    DataSet dataSet = db.transferDB.getBySerialNumber(fieldTransferCode.Text, serial);
                    int count = dataSet.Tables[0].Rows.Count;
                    if (count > 0)
                    {
                        // nothing to do.
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.getTransfer;
                        List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                        httpParams.Add(new KeyValuePair<string, string>("transfer_code", fieldTransferCode.Text));
                        httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                        httpParams.Add(new KeyValuePair<string, string>("uname_pick", controllers.userController.getUserInfo("uname")));
                        apiAgent.post(url, httpParams, parseSubmitSerialResponse, true);
                        fieldSerialNumber.Enabled = true;
                        fieldSerialNumber.Focus();
                    }
                    fieldSerialNumber.Text = "";
                }
            }
        }

        private void parseSubmitSerialResponse(string response)
        {
            models.transferModel jsonResponse = JsonConvert.DeserializeObject<models.transferModel>(response);
            if (jsonResponse.response)
            {
                db.transferDB.insertPickedSerial(fieldTransferCode.Text, fieldSerialNumber.Text, jsonResponse.location, controllers.userController.getUserInfo("uname"));
                DataSet dataSet = db.transferDB.getByTransferCode(fieldTransferCode.Text, false);
                tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
                tblTransfer.Refresh();
                btnProcess.Enabled = true;
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void fieldTransferCode_TextChanged(object sender, EventArgs e)
        {
            tblTransfer.DataSource = null;
            tblTransfer.Refresh();
            fieldSerialNumber.Enabled = false;
            btnDelete.Enabled = false;
            btnProcess.Enabled = false;
        }

        private void tblTransfer_CurrentCellChanged(object sender, EventArgs e)
        {
            tblTransfer.Select(tblTransfer.CurrentRowIndex);
            int row = tblTransfer.CurrentCell.RowNumber;
            transferCode = fieldTransferCode.Text;
            serialNumber = tblTransfer[row, 1].ToString();
            locationName = tblTransfer[row, 2].ToString();
            btnDelete.Enabled = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                tblTransfer.Select(tblTransfer.CurrentRowIndex);
                string url = controllers.serverController.getServerAddress() + apiLists.updateByDeleteTransfer;

                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                httpParams.Add(new KeyValuePair<string, string>("transfer_code", transferCode));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serialNumber));
                httpParams.Add(new KeyValuePair<string, string>("location", locationName));
                apiAgent.post(url, httpParams, parseDeleteSerialResponse, true);
            }
            btnDelete.Enabled = false;
        }

        private void parseDeleteSerialResponse(string response)
        {
            models.transferModel jsonResponse = JsonConvert.DeserializeObject<models.transferModel>(response);
            if (jsonResponse.response)
            {
                db.transferDB.deletePickedSerial(jsonResponse.transfer, jsonResponse.param);
                DataSet dataSet = db.transferDB.getByTransferCode(jsonResponse.transfer, false);
                tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
                tblTransfer.Refresh();
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    btnProcess.Enabled = true;
                }
                else
                {
                    btnProcess.Enabled = false;
                }
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            menuProsesTransfer.setTransferCode(fieldTransferCode.Text);
            menuProsesTransfer.Show();
        }

        private void formTransfer_Activated(object sender, EventArgs e)
        {
            fieldTransferCode.Focus();
            fieldSerialNumber.Enabled = false;
            string transfer = fieldTransferCode.Text;
            if (!string.IsNullOrEmpty(transfer))
            {
                DataSet dataSet = db.transferDB.getByTransferCode(fieldTransferCode.Text, false);
                tblTransfer.DataSource = controllers.transferController.getTableByDataTransfer(dataSet);
                tblTransfer.Refresh();
            }
        }
    }
}