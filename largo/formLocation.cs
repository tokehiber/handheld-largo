﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formLocation : Form
    {
        public formLocation()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void formLocation_Activated(object sender, EventArgs e)
        {
            tblLocation.DataSource = db.settingDB.getTableLocation();
            tblLocation.Refresh();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            string url = controllers.serverController.getServerAddress() + apiLists.apiSettingLocation;
            apiAgent.get(url, parseLocationResponse, true);
        }

        private void parseLocationResponse(string response)
        {
            try
            {
                models.settingModel jsonResponse = JsonConvert.DeserializeObject<models.settingModel>(response);
                if (jsonResponse.response)
                {
                    db.settingDB.truncateLocation();
                    foreach (var asdf in jsonResponse.resultLocation)
                    {
                        db.settingDB.insertLocation(asdf.loc_name, asdf.loc_desc);
                    }
                }
                else
                {
                    MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}