﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formCycleCount : Form
    {
        private string cycleCode;
        private string itemCode;
        private string serialNumber;

        public formCycleCount()
        {
            InitializeComponent();
        }

        private void setCycle(string cycle, string item, string serial)
        {
            cycleCode = cycle;
            itemCode = item;
            serialNumber = serial;
        }

        private void fieldCycleCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldCycleCode.Text))
                {
                    string cycle = fieldCycleCode.Text;
                    DataSet data = db.cycleDB.getByCycle(cycle, false);
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        cycleCode = cycle;
                        fieldSerialNumber.Enabled = true;
                        fieldSerialNumber.Focus();
                        tblCycle.DataSource = controllers.cycleController.getTableByDataCycle(data);
                        int n = db.cycleDB.getByCycle(cycle, true).Tables[0].Rows.Count;
                        if (n > 0)
                        {
                            btnSubmit.Enabled = true;
                        }
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.getCycle + cycle + "/";
                        apiAgent.get(url, parseCycleResponse, true);
                    }
                }
            }
        }

        private void parseCycleResponse(string response)
        {
            models.cycleModel jsonResponse = JsonConvert.DeserializeObject<models.cycleModel>(response);
            if (jsonResponse.response)
            {
                cycleCode = fieldCycleCode.Text;
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
                foreach (var asdf in jsonResponse.results)
                {
                    string cycle = jsonResponse.param;
                    string item = asdf.kd_barang;
                    string serial = asdf.kd_unik;
                    string location = asdf.loc_name;
                    db.cycleDB.insert(cycle, item, serial, location);
                }
                DataSet data = db.cycleDB.getByCycle(jsonResponse.param, false);
                tblCycle.DataSource = controllers.cycleController.getTableByDataCycle(data);
                int n = db.cycleDB.getByCycle(jsonResponse.param, true).Tables[0].Rows.Count;
                if (n > 0)
                {
                    btnSubmit.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldCycleCode.Text = "";
            }
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    string cycle = fieldCycleCode.Text;
                    string serial = fieldSerialNumber.Text;
                    if (db.cycleDB.getBySerial(cycle, serial).Tables[0].Rows.Count > 0)
                    {
                        db.cycleDB.update(cycle, serial, "cycle");
                    }
                    else
                    {
                        MessageBox.Show(serial + " is not valid.", "Invalid Serial Number!");
                    }
                    DataSet data = db.cycleDB.getByCycle(cycle, false);
                    tblCycle.DataSource = controllers.cycleController.getTableByDataCycle(data);
                    int n = db.cycleDB.getByCycle(cycle, true).Tables[0].Rows.Count;
                    if (n > 0)
                    {
                        btnSubmit.Enabled = true;
                    }
                    fieldSerialNumber.Text = "";
                }
            }
        }

        private void tblCycle_CurrentCellChanged(object sender, EventArgs e)
        {
            if (tblCycle[tblCycle.CurrentCell.RowNumber, 4].ToString() == "YES")
            {
                btnDelete.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
            }
            tblCycle.Select(tblCycle.CurrentRowIndex);
            string cycle = fieldCycleCode.Text;
            string item = tblCycle[tblCycle.CurrentCell.RowNumber, 1].ToString();
            string serial = tblCycle[tblCycle.CurrentCell.RowNumber, 2].ToString();
            this.setCycle(cycle, item, serial);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            db.cycleDB.update(cycleCode, serialNumber, "remove");
            DataSet data = db.cycleDB.getByCycle(cycleCode, false);
            tblCycle.DataSource = controllers.cycleController.getTableByDataCycle(data);
            tblCycle.Refresh();
            btnDelete.Enabled = false;
        }

        private void fieldCycleCode_TextChanged(object sender, EventArgs e)
        {
            fieldSerialNumber.Enabled = false;
            tblCycle.DataSource = null;
            tblCycle.Refresh();
            btnDelete.Enabled = false;
            btnSubmit.Enabled = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string cycle = fieldCycleCode.Text;
            int n = db.cycleDB.getByCycle(cycle, true).Tables[0].Rows.Count;
            int N = db.cycleDB.getByCycle(cycle, false).Tables[0].Rows.Count;
            if (n > 0 && n != N)
            {
                DialogResult konfirmasi = MessageBox.Show(cycle + ": " + n + " of " + N + " item(s) cycled.\nContinue with state some reason?", "Incomplete Counting!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formReason menuReason = new formReason();
                    menuReason.reasonFor("cycle", cycle);
                    menuReason.ShowDialog();
                    this.submit(cycle);
                }
                else
                {
                    fieldSerialNumber.Focus();
                }
            }
            else
            {
                this.submit(cycle);
            }
        }

        private void submit(string cycle)
        {
            List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
            string urlSet = controllers.serverController.getServerAddress() + apiLists.setCycle;
            string urlLock = controllers.serverController.getServerAddress() + apiLists.lockCycle;
            DataSet data = db.cycleDB.getByCycle(cycle, true);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                string cc = data.Tables[0].Rows[i]["cc_code"].ToString();
                string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                string user = data.Tables[0].Rows[i]["user_name_cc"].ToString();
                httpParams.Add(new KeyValuePair<string, string>("cc_code", cc));
                httpParams.Add(new KeyValuePair<string, string>("item_code", item));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("uname_cc", user));
                apiAgent.post(urlSet, httpParams, parseJsonSubmitBySerialResponse, true);
            }
            string reason;
            if (db.cycleDB.getReason(cycle).Tables[0].Rows.Count > 0)
            {
                reason = db.cycleDB.getReason(cycle).Tables[0].Rows[0]["reason_desc"].ToString();
            }
            else
            {
                reason = null;
            }
            
            List<KeyValuePair<string, string>> httpParamsLock = new List<KeyValuePair<string, string>>();
            httpParamsLock.Add(new KeyValuePair<string, string>("cc_code", cycle));
            httpParamsLock.Add(new KeyValuePair<string, string>("cycle_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            httpParamsLock.Add(new KeyValuePair<string, string>("reason", reason));
            apiAgent.post(urlLock, httpParamsLock, parseJsonSubmitLockResponse, true);
        }

        private void parseJsonSubmitBySerialResponse(string response)
        {
            models.cycleModel jsonResponse = JsonConvert.DeserializeObject<models.cycleModel>(response);
            if (jsonResponse.response)
            {
                db.cycleDB.remove(cycleCode, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseJsonSubmitLockResponse(string response)
        {
            models.cycleModel jsonResponse = JsonConvert.DeserializeObject<models.cycleModel>(response);
            if (jsonResponse.response)
            {
                MessageBox.Show("Data has been updated.", jsonResponse.param);
                fieldCycleCode.Text = "";
                db.cycleDB.delete(jsonResponse.param);
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }
    }
}