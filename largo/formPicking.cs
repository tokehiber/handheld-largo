﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace largo
{
    public partial class formPicking : Form
    {
        private formPickingCode menuPickingCode;
        private formBatchCode menuBatchCode;

        public formPicking()
        {
            InitializeComponent();
            menuPickingCode = new formPickingCode();
            menuBatchCode = new formBatchCode();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
            menuBatchCode.Hide();
            menuPickingCode.Hide();
        }

        private void btnPickingCode_CheckStateChanged(object sender, EventArgs e)
        {
            menuPickingCode.Show();
        }

        private void btnBatchCode_CheckStateChanged(object sender, EventArgs e)
        {
            menuBatchCode.Show();
        }

        private void formPicking_Activated(object sender, EventArgs e)
        {
            btnPickingCode.Checked = false;
            btnBatchCode.Checked = false;
        }
    }
}