﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.Net;

namespace largo
{
    public partial class formServer : Form
    {
        private static formServer instance;

        public static formServer getInstance()
        {
            if (instance == null)
            {
                instance = new formServer();
            }

            return instance;
        }

        public formServer()
        {
            InitializeComponent();
            fieldServerAddress.Text = controllers.serverController.getServerAddress();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fieldServerAddress_TextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            if (!string.IsNullOrEmpty(fieldServerAddress.Text))
            {
                btnTest.Enabled = true;
            }
            else
            {
                btnTest.Enabled = false;
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string url = fieldServerAddress.Text + apiLists.ping_server;
            List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
            httpParams.Add(new KeyValuePair<string, string>("app_name", "largo"));

            apiAgent.post(url, httpParams, parseJsonResponse, false);
        }

        private void parseJsonResponse(string response)
        {
            models.pingModel jsonResponse = JsonConvert.DeserializeObject<models.pingModel>(response);

            if (jsonResponse.response)
            {
                MessageBox.Show("Connection established");
                btnSave.Enabled = true;
                btnTest.Enabled = false;
            }
            else
            {
                btnSave.Enabled = false;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            controllers.serverController.setServerAddress(fieldServerAddress.Text);

            btnSave.Enabled = false;
            btnTest.Enabled = false;
            fieldServerAddress.Enabled = false;

            MessageBox.Show("Server address has been updated.");
        }

        private void formServer_Activated(object sender, EventArgs e)
        {
            fieldServerAddress.Enabled = true;
        }
    }
}