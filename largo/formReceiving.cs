﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formReceiving : Form
    {
        private formSerialNumber menuSerialNumber;
        private string receivingCode;
        private string itemCode;

        public formReceiving()
        {
            InitializeComponent();
            menuSerialNumber = new formSerialNumber();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            fieldReceivingCode.Text = "";
            this.Hide();
        }

        private void getReceiving(string receiving)
        {
            receivingCode = receiving;
            DataSet data = db.receivingDB.group(receivingCode);
            if (data.Tables[0].Rows.Count > 0)
            {
                tblItemCode.DataSource = controllers.receivingController.groupTableByDataReceiving(data);
                btnSubmit.Enabled = true;
            }
            else
            {
                string url = controllers.serverController.getServerAddress() + apiLists.getReceivingCode + receivingCode + "/";
                apiAgent.get(url, parseJsonReceivingResponse, true);
            }
        }

        private void fieldReceivingCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldReceivingCode.Text))
                {
                    this.getReceiving(fieldReceivingCode.Text);
                }
            }
        }

        private void parseJsonReceivingResponse(string response)
        {
            models.receivingModel jsonResponse = JsonConvert.DeserializeObject<models.receivingModel>(response);
            if (jsonResponse.response)
            {
                foreach (var asdf in jsonResponse.results)
                {
                    for (int i = 0; i < asdf.qty; i++)
                    {
                        db.receivingDB.insert(jsonResponse.param, asdf.kd_barang, asdf.st_batch, asdf.has_expdate);
                    }
                }
                DataSet data = db.receivingDB.group(receivingCode);
                tblItemCode.DataSource = controllers.receivingController.groupTableByDataReceiving(data);
                btnSubmit.Enabled = true;
            }
            else
            {
                fieldReceivingCode.Text = "";
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
                fieldReceivingCode.Focus();
                btnSubmit.Enabled = false;
            }
        }

        private void fieldReceivingCode_TextChanged(object sender, EventArgs e)
        {
            tblItemCode.DataSource = null;
            tblItemCode.Refresh();
            btnScan.Enabled = false;
            btnSubmit.Enabled = false;
        }

        private void tblItemCode_CurrentCellChanged(object sender, EventArgs e)
        {
            tblItemCode.Select(tblItemCode.CurrentRowIndex);
            btnScan.Enabled = true;
            string item = tblItemCode[tblItemCode.CurrentCell.RowNumber,1].ToString();
            menuSerialNumber.setFromReceiving(receivingCode, item);
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            menuSerialNumber.Show();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            int n = db.receivingDB.get(receivingCode, true).Tables[0].Rows.Count;
            int N = db.receivingDB.get(receivingCode, false).Tables[0].Rows.Count;
            if (n < N)
            {
                DialogResult konfirmasi = MessageBox.Show(fieldReceivingCode.Text + ": " + n + " of " + N + " item(s) scanned.\nContinue with state some reason?", "Incomplete Receiving!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    fieldReceivingCode.Text = "";
                    formReason menuReason = new formReason();
                    menuReason.reasonFor("receiving", receivingCode);
                    menuReason.ShowDialog();
                    this.submit(receivingCode);
                }
                else
                {
                    fieldReceivingCode.Focus();
                }
            }
            else
            {
                fieldReceivingCode.Text = "";
                this.submit(receivingCode);
            }
        }

        private void submit(string receiving)
        {
            DataSet data = db.receivingDB.get(receiving, true);
            string urlSet = controllers.serverController.getServerAddress() + apiLists.setReceiving;
            string urlLock = controllers.serverController.getServerAddress() + apiLists.lockReceiving;
            List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                string batch = data.Tables[0].Rows[i]["kd_batch"].ToString();
                string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                string expDate = data.Tables[0].Rows[i]["tgl_exp"].ToString();
                string inDate = data.Tables[0].Rows[i]["tgl_in"].ToString();
                string uname = data.Tables[0].Rows[i]["user_name_receiving"].ToString();
                httpParams.Add(new KeyValuePair<string, string>("receiving_code", receiving));
                httpParams.Add(new KeyValuePair<string, string>("batch_code", batch));
                httpParams.Add(new KeyValuePair<string, string>("item_code", item));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("tgl_exp", expDate));
                httpParams.Add(new KeyValuePair<string, string>("tgl_in", inDate));
                httpParams.Add(new KeyValuePair<string, string>("uname", uname));
                apiAgent.post(urlSet, httpParams, parseSubmitResponse, true);
            }
            string reason;
            if (db.receivingDB.getReason(receivingCode).Tables[0].Rows.Count > 0)
            {
                reason = db.receivingDB.getReason(receivingCode).Tables[0].Rows[0]["reason_desc"].ToString();
            }
            else
            {
                reason = null;
            }
            List<KeyValuePair<string, string>> httpParamsLock = new List<KeyValuePair<string, string>>();
            httpParamsLock.Add(new KeyValuePair<string, string>("receiving_code", receivingCode));
            httpParamsLock.Add(new KeyValuePair<string, string>("receiving_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            httpParamsLock.Add(new KeyValuePair<string, string>("reason", reason));
            apiAgent.post(urlLock, httpParamsLock, parseJsonSubmitLockResponse, true);
        }

        private void parseSubmitResponse(string response)
        {
            models.receivingModel jsonResponse = JsonConvert.DeserializeObject<models.receivingModel>(response);
            if (jsonResponse.response)
            {
                db.receivingDB.remove(receivingCode, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void parseJsonSubmitLockResponse(string response)
        {
            models.receivingModel jsonResponse = JsonConvert.DeserializeObject<models.receivingModel>(response);
            if (jsonResponse.response)
            {
                MessageBox.Show(jsonResponse.message, jsonResponse.param);
                db.receivingDB.delete(jsonResponse.param);
                db.reasonDB.delete(jsonResponse.param);
                this.Hide();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void formReceiving_Activated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(fieldReceivingCode.Text))
            {
                this.getReceiving(fieldReceivingCode.Text);
            }

            fieldReceivingCode.Focus();
            btnScan.Enabled = false;
        }

        private void fieldReceivingCode_GotFocus(object sender, EventArgs e)
        {
            btnScan.Enabled = false;
        }
    }
}