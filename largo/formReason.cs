﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace largo
{
    public partial class formReason : Form
    {
        private db.reasonDB reasonQuery;
        private string getForm;
        private string getValue;

        public formReason()
        {
            InitializeComponent();
            reasonQuery = new db.reasonDB();
        }

        public void reasonFor(string form, string value)
        {
            getForm = form;
            getValue = value;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            reasonQuery.addReason(fieldReason.Text, getForm, getValue);
            this.Close();
        }

        private void formReason_Activated(object sender, EventArgs e)
        {
            fieldReason.Focus();
        }
    }
}