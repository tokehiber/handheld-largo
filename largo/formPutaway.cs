﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formPutaway : Form
    {
        private string location;
        private string deleteSerial;

        public formPutaway()
        {
            InitializeComponent();
        }

        public void setLocation(string loc)
        {
            location = loc;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void fieldSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldSerialNumber.Text))
                {
                    if (db.putawayDB.getSerial(location, fieldSerialNumber.Text).Tables[0].Rows.Count > 0)
                    {
                        db.putawayDB.update(location, fieldSerialNumber.Text, fieldLocation.Text);
                        tblPutaway.DataSource = controllers.putawayController.getTableByDataLocation(db.putawayDB.getLocation(location));
                    }
                    else
                    {
                        MessageBox.Show(fieldSerialNumber.Text + " is not available.", "Warning!");
                    }
                    fieldSerialNumber.Text = "";
                    fieldSerialNumber.Focus();
                }
            }
        }

        private void jsonResponseSerial(string response)
        {
            models.serialNumberModel jsonResponse = JsonConvert.DeserializeObject<models.serialNumberModel>(response);
            if (jsonResponse.response)
            {
                db.putawayDB.insertPutawayToLocalDB(fieldLocation.Text, fieldSerialNumber.Text);
                tblPutaway.DataSource = db.putawayDB.getDataTable();
                btnSubmit.Enabled = true;
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
            fieldSerialNumber.Text = "";
        }

        private void fieldLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldLocation.Text))
                {
                    string location = fieldLocation.Text;
                    string url = controllers.serverController.getServerAddress() + apiLists.isValidLocation + location + "/";
                    apiAgent.get(url, jsonLocationResponse, true);
                }
            }
        }

        private void jsonLocationResponse(string response)
        {
            models.locationModel jsonResponse = JsonConvert.DeserializeObject<models.locationModel>(response);
            if (jsonResponse.response)
            {
                fieldSerialNumber.Enabled = true;
                fieldSerialNumber.Focus();
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void fieldLocation_TextChanged(object sender, EventArgs e)
        {
            fieldSerialNumber.Enabled = false;
        }

        private void formPutaway_Activated(object sender, EventArgs e)
        {
            if (db.putawayDB.getLocation(location).Tables[0].Rows.Count > 0)
            {
                // nothing to do.
            }
            else
            {
                string url = controllers.serverController.getServerAddress() + apiLists.getPutaway + location + "/";
                apiAgent.get(url, parseJsonPutawayResponse, true);
            }
            tblPutaway.DataSource = controllers.putawayController.getTableByDataLocation(db.putawayDB.getLocation(location));
        }

        private void parseJsonPutawayResponse(string response)
        {
            models.putawayModel jsonResponse = JsonConvert.DeserializeObject<models.putawayModel>(response);
            foreach (var asdf in jsonResponse.results)
            {
                string item = asdf.kd_barang;
                string serial = asdf.kd_unik;
                string loc = asdf.loc_name;
                db.putawayDB.proses(item, serial, loc);
            }
        }

        private void tblPutaway_CurrentCellChanged(object sender, EventArgs e)
        {
            tblPutaway.Select(tblPutaway.CurrentRowIndex);
            btnSubmit.Enabled = true;
            if (!string.IsNullOrEmpty(tblPutaway[tblPutaway.CurrentCell.RowNumber, 3].ToString()))
            {
                deleteSerial = tblPutaway[tblPutaway.CurrentCell.RowNumber, 2].ToString();
                btnDelete.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult konfirmasi = MessageBox.Show("Are you sure to delete " + deleteSerial + " from list?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
            if (konfirmasi == DialogResult.Yes)
            {
                db.putawayDB.remove(location, deleteSerial);
                tblPutaway.DataSource = controllers.putawayController.getTableByDataLocation(db.putawayDB.getLocation(location));
            }
            else
            {
                fieldLocation.Focus();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            DataSet dataSet = db.putawayDB.getLocation(location);
            if (dataSet.Tables[0].Rows.Count > 0 && db.putawayDB.getLocationNew(location).Tables[0].Rows.Count > 0)
            {
                string url = controllers.serverController.getServerAddress() + apiLists.setPutaway;
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    string serial = dataSet.Tables[0].Rows[i]["kd_unik"].ToString();
                    string old = dataSet.Tables[0].Rows[i]["loc_name"].ToString();
                    string loc = dataSet.Tables[0].Rows[i]["loc_name_new"].ToString();
                    string uname = dataSet.Tables[0].Rows[i]["user_name_putaway"].ToString();
                    List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                    httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                    httpParams.Add(new KeyValuePair<string, string>("loc_name", loc));
                    httpParams.Add(new KeyValuePair<string, string>("uname", uname));
                    if (!string.IsNullOrEmpty(loc))
                    {
                        apiAgent.post(url, httpParams, jsonResponseSubmit, true);
                    }
                    else
                    {
                        db.putawayDB.delete(old, serial);
                    }
                }
                MessageBox.Show("Data has been updated.", "Putaway Information");
                this.Hide();
            }
            else
            {
                MessageBox.Show("List of putaway are empty.", "Warning!");
                btnSubmit.Enabled = false;
                fieldLocation.Focus();
            }
        }

        private void jsonResponseSubmit(string response)
        {
            models.serialNumberModel jsonResponse = JsonConvert.DeserializeObject<models.serialNumberModel>(response);
            if (jsonResponse.response)
            {
                db.putawayDB.delete(location, jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void fieldLocation_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }

        private void fieldSerialNumber_GotFocus(object sender, EventArgs e)
        {
            btnDelete.Enabled = false;
        }
    }
}