﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace largo
{
    public partial class formReceiving : Form
    {
        private formSerialNumber menuSerialNumber;

        public formReceiving()
        {
            InitializeComponent();
            menuSerialNumber = new formSerialNumber();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            fieldReceivingCode.Text = "";
            fieldReceivingCode.Enabled = true;
            this.Hide();
        }

        private void fieldReceivingCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (!string.IsNullOrEmpty(fieldReceivingCode.Text))
                {
                    string receivingCode = fieldReceivingCode.Text;
                    int count = db.receivingDB.countByReceivingCode(receivingCode);
                    if (count > 0)
                    {
                        tblItemCode.DataSource = db.receivingDB.getTableByReceivingCode(receivingCode);
                        fieldReceivingCode.Enabled = false;
                    }
                    else
                    {
                        string url = controllers.serverController.getServerAddress() + apiLists.getReceivingCode + receivingCode + "/";
                        apiAgent.get(url, parseJsonResponse, true);
                        tblItemCode.DataSource = db.receivingDB.getTableByReceivingCode(receivingCode);
                    }
                }
            }
        }

        private void parseJsonResponse(string response)
        {
            models.receivingModel jsonResponse = JsonConvert.DeserializeObject<models.receivingModel>(response);
            if (jsonResponse.response)
            {
                foreach (var asdf in jsonResponse.results)
                {
                    for (int i = 0; i < asdf.qty; i++)
                    {
                        db.receivingDB.insertReceivingToLocalDB(asdf.kd_receiving, asdf.kd_barang, asdf.has_expdate.ToString());
                        //fieldReceivingCode.Enabled = false;
                    }
                }
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }

        private void fieldReceivingCode_TextChanged(object sender, EventArgs e)
        {
            tblItemCode.DataSource = null;
            tblItemCode.Refresh();
            btnScan.Enabled = false;
            btnSubmit.Enabled = false;
        }

        private void tblItemCode_CurrentCellChanged(object sender, EventArgs e)
        {
            tblItemCode.Select(tblItemCode.CurrentRowIndex);
            btnScan.Enabled = true;
            btnSubmit.Enabled = true;
            int row = tblItemCode.CurrentCell.RowNumber;
            menuSerialNumber.setFromReceiving(fieldReceivingCode.Text, tblItemCode[row,1].ToString(), "formReceiving");
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            menuSerialNumber.Show();
        }

        private void formReceiving_Activated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(fieldReceivingCode.Text))
            {
                tblItemCode.DataSource = db.receivingDB.getTableByReceivingCode(fieldReceivingCode.Text);
                btnScan.Enabled = false;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            int N = db.receivingDB.countByReceivingCode(fieldReceivingCode.Text);
            int n = db.receivingDB.countBySerialOnReceiving(fieldReceivingCode.Text);
            if (n < N)
            {
                DialogResult konfirmasi = MessageBox.Show(fieldReceivingCode.Text + ": " + n + " of " + N + " item(s) scanned.\nContinue with state some reason?", "Incomplete Receiving!", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
                if (konfirmasi == DialogResult.Yes)
                {
                    formReason menuReason = new formReason();
                    menuReason.reasonFor("receiving", fieldReceivingCode.Text);
                    menuReason.ShowDialog();
                    this.submitReceiving(fieldReceivingCode.Text);
                    this.Hide();
                }
            }
            else
            {
                this.submitReceiving(fieldReceivingCode.Text);
                this.Hide();
            }
            btnSubmit.Enabled = false;
        }

        private void submitReceiving(string receiving)
        {
            DataSet dataSet = db.receivingDB.getReceiving(receiving);
            string url = controllers.serverController.getServerAddress() + apiLists.setReceiving;
            int count = dataSet.Tables[0].Rows.Count;
            for (int i = 0; i < count; i++)
            {
                string barang = dataSet.Tables[0].Rows[i]["kd_barang"].ToString();
                string serial = dataSet.Tables[0].Rows[i]["kd_unik"].ToString();
                string expDate = dataSet.Tables[0].Rows[i]["tgl_exp"].ToString();
                string dateIn = dataSet.Tables[0].Rows[i]["tgl_in"].ToString();
                string uname = dataSet.Tables[0].Rows[i]["user_name_receiving"].ToString();
                string remark = dataSet.Tables[0].Rows[i]["remark"].ToString();
                List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
                httpParams.Add(new KeyValuePair<string, string>("receiving_code", receiving));
                httpParams.Add(new KeyValuePair<string, string>("item_code", barang));
                httpParams.Add(new KeyValuePair<string, string>("serial_number", serial));
                httpParams.Add(new KeyValuePair<string, string>("tgl_exp", expDate));
                httpParams.Add(new KeyValuePair<string, string>("tgl_in", dateIn));
                httpParams.Add(new KeyValuePair<string, string>("uname", uname));
                httpParams.Add(new KeyValuePair<string, string>("remark", remark));
                apiAgent.post(url, httpParams, parseJsonSubmitResponse, true);
            }
            db.receivingDB.deleteByReceivingCode(receiving);
        }

        private void parseJsonSubmitResponse(string response)
        {
            models.receivingModel jsonResponse = JsonConvert.DeserializeObject<models.receivingModel>(response);
            if (jsonResponse.response)
            {
                db.receivingDB.deleteBySerialNumber(jsonResponse.param);
            }
            else
            {
                MessageBox.Show(jsonResponse.message, "Error: " + jsonResponse.status);
            }
        }
    }
}