﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Win32;

namespace largo
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Registry.CurrentUser.DeleteSubKey(@"SOFTWARE\largo\user_info", false);
            // Registry.CurrentUser.DeleteSubKey(@"SOFTWARE\largo\server_info", false);

            // membuka registry session_code
            string sessionCode = controllers.userController.getUserInfo("session_code");

            if (!string.IsNullOrEmpty(sessionCode))
            {
                // run formMenu saat session_code ada di registry
                Application.Run(formMenu.getInstance());
            }
            else
            {
                // run formLogin untuk login
                Application.Run(formLogin.getInstance());
            }
        }
    }
}