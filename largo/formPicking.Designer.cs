﻿namespace largo
{
    partial class formPicking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPickingCode = new System.Windows.Forms.CheckBox();
            this.btnBatchCode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 24);
            this.label1.Text = "Picking";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(16, 16);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "X";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label2.Location = new System.Drawing.Point(3, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(234, 20);
            this.label2.Text = "Choose picking method:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnPickingCode
            // 
            this.btnPickingCode.Location = new System.Drawing.Point(58, 154);
            this.btnPickingCode.Name = "btnPickingCode";
            this.btnPickingCode.Size = new System.Drawing.Size(125, 20);
            this.btnPickingCode.TabIndex = 0;
            this.btnPickingCode.Text = "By picking code";
            this.btnPickingCode.CheckStateChanged += new System.EventHandler(this.btnPickingCode_CheckStateChanged);
            // 
            // btnBatchCode
            // 
            this.btnBatchCode.Location = new System.Drawing.Point(58, 180);
            this.btnBatchCode.Name = "btnBatchCode";
            this.btnBatchCode.Size = new System.Drawing.Size(125, 20);
            this.btnBatchCode.TabIndex = 1;
            this.btnBatchCode.Text = "By batch code";
            this.btnBatchCode.CheckStateChanged += new System.EventHandler(this.btnBatchCode_CheckStateChanged);
            // 
            // formPicking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.btnBatchCode);
            this.Controls.Add(this.btnPickingCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formPicking";
            this.Text = "Menu Picking";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.formPicking_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox btnPickingCode;
        private System.Windows.Forms.CheckBox btnBatchCode;
    }
}