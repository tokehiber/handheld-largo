﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class receivingController
    {
        public static DataTable groupTableByDataReceiving(DataSet data)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("QTY", typeof(string));
                int no = 0;
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    no += 1;
                    string receiving = data.Tables[0].Rows[i]["kd_receiving"].ToString();
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    string n = db.receivingDB.getByItem(receiving, item, true).Tables[0].Rows.Count.ToString();
                    string N = db.receivingDB.getByItem(receiving, item, false).Tables[0].Rows.Count.ToString();
                    string qty = n + "/" + N;
                    dataTable.Rows.Add(no, item, qty);
                }
                return dataTable;
            }
        }
    }
}
