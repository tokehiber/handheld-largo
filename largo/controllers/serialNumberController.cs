﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class serialNumberController
    {
        public static DataTable getTableByDataItem(DataSet data)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Batch Code", typeof(string));
                dataTable.Columns.Add("Exp Date", typeof(string));
                int no = 0;
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    no += 1;
                    string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                    string batch = data.Tables[0].Rows[i]["kd_batch"].ToString();
                    string expdate = data.Tables[0].Rows[i]["tgl_exp"].ToString();
                    dataTable.Rows.Add(no, serial, batch, expdate);
                }
                return dataTable;
            }
        }
    }
}
