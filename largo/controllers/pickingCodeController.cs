﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class pickingCodeController
    {
        public static DataTable getByPickingCode(string picking)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("QTY", typeof(string));
                DataSet data = db.pickingCodeDB.getGroupByItem(picking);
                int count = data.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    int n = db.pickingCodeDB.getByItem(picking, item, true).Tables[0].Rows.Count;
                    int N = db.pickingCodeDB.getByItem(picking, item, false).Tables[0].Rows.Count;
                    string qty = n + "/" + N;
                    dataTable.Rows.Add(no, item, qty);
                }
                return dataTable;
            }
        }

        public static DataTable getLocationByPickingCode(string picking)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("Location", typeof(string));
                dataTable.Columns.Add("QTY", typeof(int));
                DataSet data = db.pickingCodeDB.getLocation(picking);
                int count = data.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    string location = data.Tables[0].Rows[i]["loc_name"].ToString();
                    int qty = Convert.ToInt32(data.Tables[0].Rows[i]["qty"].ToString());
                    dataTable.Rows.Add(no, item, location, qty);
                }
                return dataTable;
            }
        }

        public static DataTable getByPickingItemCode(string picking, string item)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("Serial Number", typeof(string));
                DataSet data = db.pickingCodeDB.getByItem(picking, item, true);
                int count = data.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                    dataTable.Rows.Add(no, item, serial);
                }
                return dataTable;
            }
        }
    }
}
