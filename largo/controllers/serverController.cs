﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace largo.controllers
{
    class serverController
    {
        public static string getServerAddress()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\largo\server_info", false);
            if (regKey != null)
            {
                return regKey.GetValue("server_address").ToString();
            }
            else
            {
                return "";
            }
        }

        public static void setServerAddress(string server)
        {
            RegistryKey registry = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\largo\server_info");
            registry.SetValue("server_address", server);
            registry.Close();
        }
    }
}
