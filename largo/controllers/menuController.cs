﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace largo.controllers
{
    class menuController
    {
        private bool isValid;

        public bool isUserValid()
        {
            this.getUserValidation();
            return isValid;
        }

        private void getUserValidation()
        {
            string uname = controllers.userController.getUserInfo("uname");
            string session_code = controllers.userController.getUserInfo("session_code");
            string url = controllers.serverController.getServerAddress() + apiLists.userValidator;
            List<KeyValuePair<string, string>> httpParams = new List<KeyValuePair<string, string>>();
            httpParams.Add(new KeyValuePair<string, string>("uname", uname));
            httpParams.Add(new KeyValuePair<string, string>("session_code", session_code));

            apiAgent.post(url, httpParams, parseJsonResponse, false);
        }

        private void parseJsonResponse(string response)
        {
            models.menuModel jsonResponse = JsonConvert.DeserializeObject<models.menuModel>(response);
            isValid = jsonResponse.response;
        }
    }
}
