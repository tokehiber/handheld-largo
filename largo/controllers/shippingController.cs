﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class shippingController
    {
        public static DataTable getTableByDataShipping(DataSet data)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Shipped", typeof(string));
                int no = 0;
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    no += 1;
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                    string status = data.Tables[0].Rows[i]["st_ship"].ToString();
                    dataTable.Rows.Add(no, item, serial, status);
                }
                return dataTable;
            }
        }
    }
}
