﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;

namespace largo.controllers
{
    class userController
    {
        public static string getUserInfo(string info)
        {
            try
            {
                RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\largo\user_info", false);
                string value = regKey.GetValue(info).ToString();
                regKey.Close();
                return value;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static void removeSession()
        {
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\largo\user_info", true);
            regKey.SetValue("uid", "");
            regKey.SetValue("uname", "");
            regKey.SetValue("session_code", "");
            regKey.Close();
        }

        public static void setUserInfo(string uid, string uname, string sessionCode)
        {
            RegistryKey registry = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\largo\user_info");
            registry.SetValue("uid", uid);
            registry.SetValue("uname", uname);
            registry.SetValue("session_code", sessionCode);
            registry.Close();
        }
    }
}
