﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class cycleController
    {
        public static DataTable getTableByDataCycle(DataSet data)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Item Code", typeof(string));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Location", typeof(string));
                dataTable.Columns.Add("Cycled", typeof(string));
                int no = 0;
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    no += 1;
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                    string location = data.Tables[0].Rows[i]["loc_name"].ToString();
                    string status = data.Tables[0].Rows[i]["st_cc"].ToString();
                    dataTable.Rows.Add(no, item, serial, location, status);
                }
                return dataTable;
            }
        }
    }
}
