﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class putawayController
    {
        public static DataTable getTableByDataLocation(DataSet data)
        {
            using (DataTable dataTabel = new DataTable())
            {
                dataTabel.Columns.Add("No.", typeof(int));
                dataTabel.Columns.Add("Item Code", typeof(string));
                dataTabel.Columns.Add("Serial Number", typeof(string));
                dataTabel.Columns.Add("Location", typeof(string));
                int no = 0;
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    no += 1;
                    string item = data.Tables[0].Rows[i]["kd_barang"].ToString();
                    string serial = data.Tables[0].Rows[i]["kd_unik"].ToString();
                    string loc = data.Tables[0].Rows[i]["loc_name_new"].ToString();
                    dataTabel.Rows.Add(no, item, serial, loc);
                }
                return dataTabel;
            }
        }
    }
}
