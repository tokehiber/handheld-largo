﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace largo.controllers
{
    class transferController
    {
        public static DataTable getTableByDataTransfer(DataSet dataTransfer)
        {
            using (DataTable dataTable = new DataTable())
            {
                dataTable.Columns.Add("No.", typeof(int));
                dataTable.Columns.Add("Serial Number", typeof(string));
                dataTable.Columns.Add("Old Loc", typeof(string));
                dataTable.Columns.Add("New Loc", typeof(string));
                int count = dataTransfer.Tables[0].Rows.Count;
                int no = 0;
                for (int i = 0; i < count; i++)
                {
                    no += 1;
                    string serial = dataTransfer.Tables[0].Rows[i]["kd_unik"].ToString();
                    string oldLoc = dataTransfer.Tables[0].Rows[i]["loc_name_old"].ToString();
                    string newLoc = dataTransfer.Tables[0].Rows[i]["loc_name_new"].ToString();
                    dataTable.Rows.Add(no, serial, oldLoc, newLoc);
                }
                return dataTable;
            }
        }
    }
}
